<?php 
	session_start();
	include('koneksi/koneksi.php');
	if(isset($_GET["include"])){
		$include = $_GET["include"];
		if($include=="konfirmasi_login"){
			include("include/konfirmasi_login.php");
		}else if($include=="singout"){
			include("include/singout.php");
		}else if($include=="konfirmasi_tambah_cabang"){
			include("include/konfirmasi_tambah_cabang.php");
		}else if($include=="konfirmasi_edit_cabang"){
			include("include/konfirmasi_edit_cabang.php");
		}else if($include=="konfirmasi_tambah_cuti"){
			include("include/konfirmasi_tambah_cuti.php");
		}else if($include=="konfirmasi_edit_cuti"){
			include("include/konfirmasi_edit_cuti.php");
		}else if($include=="konfirmasi_tambah_jabatan"){
			include("include/konfirmasi_tambah_jabatan.php");
		}else if($include=="konfirmasi_edit_jabatan"){
			include("include/konfirmasi_edit_jabatan.php");
		}else if($include=="konfirmasi_tambah_staff"){
			include("include/konfirmasi_tambah_staff.php");
		}else if($include=="konfirmasi_edit_staff"){
			include("include/konfirmasi_edit_staff.php");
		}else if($include=="konfirmasi_tambah_user"){
			include("include/konfirmasi_tambah_user.php");
		}else if($include=="konfirmasi_edit_user"){
			include("include/konfirmasi_edit_user.php");
		}else if($include=="konfirmasi_edit_profil"){
			include("include/konfirmasi_edit_profil.php");
		}else if($include=="konfirmasi_tambah_karyawan"){
			include("include/konfirmasi_tambah_karyawan.php");
		}else if($include=="konfirmasi_edit_karyawan"){
			include("include/konfirmasi_edit_karyawan.php");
		}else if($include=="konfirmasi_tambah_lamaran"){
			include("include/konfirmasi_tambah_lamaran.php");
		}else if($include=="konfirmasi_edit_lamaran"){
			include("include/konfirmasi_edit_lamaran.php");
		}else if($include=="konfirmasi_tambah_departemen"){
			include("include/konfirmasi_tambah_departemen.php");
		}else if($include=="konfirmasi_edit_departemen"){
			include("include/konfirmasi_edit_departemen.php");
		}
	}
?>
<html>
	<head>
		<?php include("includes/head.login.php")?>
	</head>
	<?php
		//jika ada get include
		if(isset($_GET["include"])){
		  $include = $_GET["include"];
		  //jika ada session id admin
			if(isset($_SESSION['id_admin'])){
			//pemanggilan ke halaman-halaman menu admin
	?>
			<body>
				<!-- ============================================================== -->
				<!-- Preloader - style you can find in spinners.css -->
				<!-- ============================================================== -->
				<div class="preloader">
					<div class="lds-ripple">
						<div class="lds-pos"></div>
						<div class="lds-pos"></div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- Main wrapper - style you can find in pages.scss -->
				<!-- ============================================================== -->
				<div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full"data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
					<!-- ============================================================== -->
					<!-- Topbar header - style you can find in pages.scss -->
					<!-- ============================================================== -->
					<?php include("includes/header.php")?>
					<!-- ============================================================== -->
					<!-- End Topbar header -->
					<!-- ============================================================== -->
					<!-- ============================================================== -->
					<!-- Left Sidebar - style you can find in sidebar.scss  -->
					<!-- ============================================================== -->
					<?php include("includes/sidebar.php")?>
					<!-- ============================================================== -->
					<!-- End Left Sidebar - style you can find in sidebar.scss  -->
					<!-- ============================================================== -->
					<!-- ============================================================== -->
					<!-- Page wrapper  -->
					<!-- ============================================================== -->
					<div class="page-wrapper">
						<?php
							if($include=="profil"){
								include("include/profil.php");
							}else if($include=="edit_profil"){
								include("include/edit_profil.php");
							}else if($include=="cabang"){
								include("include/cabang.php");
							}else if($include=="tambah_cabang"){
								include("include/tambah_cabang.php");
							}else if($include=="edit_cabang"){
								include("include/edit_cabang.php");
							}else if($include=="cuti"){
								include("include/cuti.php");
							}else if($include=="tambah_cuti"){
								include("include/tambah_cuti.php");
							}else if($include=="edit_cuti"){
								include("include/edit_cuti.php");
							}else if($include=="jabatan"){
								include("include/jabatan.php");
							}else if($include=="tambah_jabatan"){
								include("include/tambah_jabatan.php");
							}else if($include=="edit_jabatan"){
								include("include/edit_jabatan.php");
							}else if($include=="staff"){
								include("include/staff.php");
							}else if($include=="tambah_staff"){
								include("include/tambah_staff.php");
							}else if($include=="edit_staff"){
								include("include/edit_staff.php");
							}else if($include=="user"){
								include("include/user.php");
							}else if($include=="tambah_user"){
								include("include/tambah_user.php");
							}else if($include=="edit_user"){
								include("include/edit_user.php");
							}else if($include=="karyawan"){
								include("include/karyawan.php");
							}else if($include=="tambah_karyawan"){
								include("include/tambah_karyawan.php");
							}else if($include=="edit_karyawan"){
								include("include/edit_karyawan.php");
							}else if($include=="detail_karyawan"){
								include("include/detail_karyawan.php");
							}else if($include=="lamaran"){
								include("include/lamaran.php");
							}else if($include=="tambah_lamaran"){
								include("include/tambah_lamaran.php");
							}else if($include=="edit_lamaran"){
								include("include/edit_lamaran.php");
							}else if($include=="karyawan_baru"){
								include("include/KARYAWAN_BARU.php");
							}else if($include=="detail_karyawan_baru"){
								include("include/detail_karyawan_baru.php");
							}else if($include=="departemen"){
								include("include/departemen.php");
							}else if($include=="tambah_departemen"){
								include("include/tambah_departemen.php");
							}else if($include=="edit_departemen"){
								include("include/edit_departemen.php");
							}else{
								include("include/dashboard.php");
							}
						?>
						<!-- footer -->
						<!-- ============================================================== -->
						<?php include("includes/footer.php")?>
						<!-- ============================================================== -->
						<!-- End footer -->
						<!-- ============================================================== -->
					</div>
					<!-- ============================================================== -->
					<!-- End Page wrapper  -->
					<!-- ============================================================== -->
				</div>
				<!-- ============================================================== -->
				<!-- End Wrapper -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- All Jquery -->
				<!-- ============================================================== -->
				<?php include("includes/script.php")?>
			</body>
	<?php    
			}
		}else{
			 //jika tidak ada include pemanggilan halaman form login
			include("include/login.php");
		}
	?>
</html>