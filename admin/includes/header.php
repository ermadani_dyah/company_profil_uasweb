<header class="topbar">
	<nav class="navbar top-navbar navbar-expand-md navbar-dark">
		<div class="navbar-header">
				<!-- ============================================================== -->
				<!-- Logo -->
				<!-- ============================================================== -->
				<a class="navbar-brand justify-content-center" href="index.php">
					<!-- Logo icon -->
					<b class="logo-icon">
						<!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
						<!-- Dark Logo icon -->
						<img src="assets/images/icon8.png" alt="homepage" class="dark-logo" />
					</b>
					<!--End Logo icon -->
					<!-- Logo text -->
					<span class="logo-text">
						<!-- dark Logo text -->
						<img src="assets/images/ACE9.png" alt="homepage" class="dark-logo" />
					</span>
				</a>
				<!-- ============================================================== -->
				<!-- End Logo -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- toggle and nav items -->
				<!-- ============================================================== -->
				<a class="nav-toggler waves-effect waves-light text-dark d-block d-md-none" href="javascript:void(0)">
					<i class="ti-menu ti-close"></i>
				</a>
		</div>
		<!-- ============================================================== -->
		<!-- End Logo -->
		<!-- ============================================================== -->
		<div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
			<ul class="navbar-nav d-none d-md-block d-lg-none">
				<li class="nav-item">
					<a class="nav-toggler nav-link waves-effect waves-light text-white"href="javascript:void(0)">
						<i class="ti-menu ti-close"></i>
					</a>
				</li>
			</ul>
			<!-- ============================================================== -->
			<!-- toggle and nav items -->
			<!-- ============================================================== -->
			<ul class="navbar-nav mr-auto mt-md-0 ">
				<li class="nav-item d-none d-sm-inline-block">
					<a href="index.php" class="nav-link">
						<i class="mr-3 fas fa-home" aria-hidden="true"></i>
						Home
					</a>
				</li>
				<!--<li class="nav-item d-none d-sm-inline-block">
				<a href="#" class="nav-link">Contact</a>
				</li>-->
			</ul>
			<!-- ============================================================== -->
			<!-- Right side toggle and nav items -->
			<!-- ============================================================== -->
			<ul class="navbar-nav">
				<!-- ============================================================== -->
				<!-- User profile and search -->
				<!-- ============================================================== -->
				<li class="nav-item">
					<a href="index.php?include=singout" class="nav-link">
						Sign Out&nbsp; 
						<i class="fas fa-sign-out-alt"></i>
					</a>
				</li>
			</ul>
		</div>
	</nav>
</header>