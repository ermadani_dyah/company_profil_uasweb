<aside class="left-sidebar sidebar-dark-primary elevation-4" data-sidebarbg="skin6">
	<!-- Sidebar scroll-->
	<div class="scroll-sidebar">
	<!-- Sidebar navigation-->
		<nav class="sidebar-nav">
			<ul id="sidebarnav">
				<!-- User Profile-->
				<li class="sidebar-item"> 
					<a class="sidebar-link waves-effect waves-dark sidebar-link" href="index.php?include=dashboard" aria-expanded="false">
						<i class="mr-3 fas fa-align-justify" aria-hidden="true"></i>
						<span class="hide-menu">Dashboard</span>
					</a>
				</li>
				<li class="sidebar-item"> 
					<a class="sidebar-link waves-effect waves-dark sidebar-link" href="index.php?include=profil" aria-expanded="false">
						<i class="mr-3 fa fa-user" aria-hidden="true"></i>
						<span class="hide-menu">Profile</span>
					</a>
				</li>
				<li class="sidebar-item"> 
					<a class="sidebar-link waves-effect waves-dark sidebar-link" href="index.php?include=cabang" aria-expanded="false">
						<i class="mr-3 fas fa-building" aria-hidden="true"></i>
						<span class="hide-menu">Cabang</span>
					</a>
				</li>
				<li class="sidebar-item"> 
					<a class="sidebar-link waves-effect waves-dark sidebar-link" href="index.php?include=jabatan" aria-expanded="false">
						<i class="mr-3 fa fas fa-id-card-alt" aria-hidden="true"></i>
						<span class="hide-menu">Jabatan</span>
					</a>
				</li>
				<li class="sidebar-item"> 
					<a class="sidebar-link waves-effect waves-dark sidebar-link" href="index.php?include=karyawan" aria-expanded="false">
						<i class="mr-3 fas fa-users" aria-hidden="true"></i>
						<span class="hide-menu">Karyawan</span>
					</a>
				</li>
				<li class="sidebar-item"> 
					<a class="sidebar-link waves-effect waves-dark sidebar-link" href="index.php?include=cuti" aria-expanded="false">
						<i class="mr-3 fas fa-street-view"aria-hidden="true"></i>
						<span class="hide-menu">Cuti</span>
					</a>
				</li>
				<li class="sidebar-item"> 
					<a class="sidebar-link waves-effect waves-dark sidebar-link" href="index.php?include=departemen" aria-expanded="false">
						<i class="mr-3 fas fa-diagnoses"aria-hidden="true"></i>
						<span class="hide-menu">Departemen</span>
					</a>
				</li>
				<li class="sidebar-item"> 
					<a class="sidebar-link waves-effect waves-dark sidebar-link" href="index.php?include=karyawan_baru" aria-expanded="false">
						<i class="mr-3 fas fa-graduation-cap"aria-hidden="true"></i>
						<span class="hide-menu">Karyawan Baru</span>
					</a>
				</li>
				<li class="sidebar-item"> 
					<a class="sidebar-link waves-effect waves-dark sidebar-link" href="index.php?include=lamaran" aria-expanded="false">
						<i class="mr-3 fas fa-globe"aria-hidden="true"></i>
						<span class="hide-menu">Lamaran Pekerjaan</span>
					</a>
				</li>
				<?php 
				if (isset($_SESSION['level'])){
					if ($_SESSION['level']=="kepala"){?>
						<li class="sidebar-item"> 
							<a class="sidebar-link waves-effect waves-dark sidebar-link dropdown-toggle" data-toggle="dropdown" href="user.php" aria-expanded="false">
								<i class="mr-3 fas fa-fw fa-cog" aria-hidden="true"></i>
								<span class="hide-menu">Pengaturan</span>
							</a>
							<div class="dropdown-menu">
								<div class="dropdown-header">Pengaturan Anggota</div>
								<a class="dropdown-item " href="index.php?include=user">Pengaturan User</a>
								<a class="dropdown-item" href="index.php?include=staff">Pengaturan Staff</a>
							</div>
						</li>
					<?php }?>
				<?php }?>
				<li class="sidebar-item"> 
					<a class="sidebar-link waves-effect waves-dark sidebar-link"href="index.php?include=singout" aria-expanded="false">
						<i class="mr-3 fas fa-sign-out-alt"aria-hidden="true"></i>
						<span class="hide-menu">Sing Out</span>
					</a>
				</li>
			</ul>
		</nav>
		<!-- End Sidebar navigation -->
	</div>
	<!-- End Sidebar scroll-->
</aside>