<body>
	<div id="login">
		<div class="container">
			<h1 class="text-center text-white pt-5">PT LANCAR JAYA</h1>
			<div id="login-row" class="row justify-content-center align-items-center">
				<div id="login-column" class="col-md-6">
					<div id="login-box" class="col-md-12">
						<form id="login-form" class="form" action="index.php?include=konfirmasi_login" method="post">
							<br>
							<h3 class="text-center text-info">LOGIN</h3>
							<?php if(!empty($_GET['gagal'])){?>
								<?php if($_GET['gagal']=="userKosong"){?>
									<span class="text-danger">Maaf Username Tidak Boleh Kosong</span>
								<?php } else if($_GET['gagal']=="passKosong"){?>
									<span class="text-danger">Maaf Password Tidak Boleh Kosong</span>
								<?php } else {?>
									<span class="text-danger">Maaf Username dan Password Anda Salah</span>
								<?php }?>
									<br>
							<?php }?>
							<div class="form-group">
								<label for="username" class="text-info">Username:</label><br>
								<input type="username" name="username" id="username" class="form-control">
							</div>
							<div class="form-group">
								<label for="password" class="text-info">Password:</label><br>
								<input type="password" name="password" id="password" class="form-control">
							</div>
							<div class="col-4">
								<button type="submit" id="ll" name="login" value="Login" class="btn btn-primary center-block">Login</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>