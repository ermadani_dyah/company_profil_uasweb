<?php 
	if(isset($_GET['data'])){
		$id_admin = $_GET['data'];
		$_SESSION['id_admin']=$id_admin;
		//get data user
		$sql_m = "select `nama`,`nama_lengkap`, `email`,`no_hp`, `username`,`password`,`level` from `admin` where `id_admin` = '$id_admin'";
		$query_m = mysqli_query($koneksi,$sql_m);
		while($data_m = mysqli_fetch_row($query_m)){
			$nama= $data_m[0];
			$nama_lengkap= $data_m[1];
			$email = $data_m[2];
			$no_hp = $data_m[3];
			$username= $data_m[4];
			$password = $data_m[5];
			$level= $data_m[6];
		} 
	}
?>
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">
                <i class="mr-3 fas fa-pencil-alt" aria-hidden="true"></i>
                Edit User
            </h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.php?include=dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">User</li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Data User</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-right upgrade-btn">
                <a href="index.php?include=user" class="btn btn-sm btn-warning d-none d-md-inline-block text-white">
                    <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- column -->
        <div class="col-sm-12">
            <div class="card">
                <div class="row">
                    <!-- column -->
                    <div class="col-sm-12">
                        <div class="card-body">
                            <section class="content">
                                <div class="card card-info">
                                    <div class="card-header">
                                        <h3 class="card-title" style="margin-top:5px;">
                                            <i class="far fa-list-alt"></i> Form Edit User
                                        </h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <br>
                                    <div class="col-sm-10">
                                        <?php if((!empty($_GET['notif']))&&(!empty($_GET['jenis']))){?>
                                        <?php if($_GET['notif']=="editkosong"){?>
                                        <div class="alert alert-danger" role="alert">
                                            Maaf data
                                            <?php echo $_GET['jenis'];?> wajib di isi
                                        </div>
                                        <?php }?>
                                        <?php }?>
                                    </div>
                                    <form class="form-horizontal" method="post" action="index.php?include=konfirmasi_edit_user">
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label for="nama" class="col-sm-3 col-form-label">Nama Lengap</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="nama" name="nama" value="<?php echo $nama_lengkap;?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="nama" class="col-sm-3 col-form-label">Nama panggilan</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="panggilan" name="panggilan" value="<?php echo $nama;?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="email" class="col-sm-3 col-form-label">Email</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="email" name="email" value="<?php echo $email;?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="email" class="col-sm-3 col-form-label">No Hp</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="hp" name="hp" value="<?php echo $no_hp;?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="username" class="col-sm-3 col-form-label">Username</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="username" name="username" value="<?php echo $username;?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="password" class="col-sm-3 col-form-label">Password</label>
                                                <div class="col-sm-7">
                                                    <input type="password" class="form-control" id="password" name="password" value="<?php echo $password;?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="level" class="col-sm-3 col-form-label">Level</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="level" name="level" value="<?php echo $level;?>">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-info float-right">
                                                <i class="mr-3  far fa-save" aria-hidden="true"></i>Simpan
                                            </button>
                                        </div>
                                        <!-- /.card-footer -->
                                    </form>
                                </div>
                                <!-- /.card -->
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->