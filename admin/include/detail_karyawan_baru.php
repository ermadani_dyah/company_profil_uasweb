<?php 
	if(isset($_GET['data'])){
		$nik = $_GET['data'];
		//get data karyawan
		$sql_m = "SELECT * from `karyawan_baru` where `nik`='$nik'";
		$query_m = mysqli_query($koneksi,$sql_m);
		while($data_m = mysqli_fetch_row($query_m)){
			$nik = $data_m[0];
            $nama = $data_m[1];
            $jk=$data_m[2];
            $pendidikan=$data_m[3];
            $tempat = $data_m[4];
            $pengalaman=$data_m[5];
            $tanggal = $data_m[6];
            $email=$data_m[7];
            $agama = $data_m[8];
            $no_hp=$data_m[9];
		}	
	}
?>
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">
                <i class="mr-3 fas fa-pencil-alt" aria-hidden="true"></i>
                Detail Data Karyawan baru
            </h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.php?include=dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Data Karyawan baru</li>
                        <li class="breadcrumb-item active" aria-current="page">Detail Data Karyawan baru</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-right upgrade-btn">
                <a href="index.php?include=karyawan_baru" class="btn btn-sm btn-warning d-none d-md-inline-block text-white">
                    <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- column -->
        <div class="col-sm-12">
            <div class="card">
                <div class="row">
                    <!-- column -->
                    <div class="col-sm-12">
                        <div class="card-body">
                            <section class="content">
                                <div class="card card-info">
                                    <div class="card-header">
                                        <h3 class="card-title" style="margin-top:5px;">
                                            <i class="far fa-list-alt"></i> Form Data Karyawan Baru
                                        </h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <br>
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <i class="fas fa-user-circle"></i>
                                                    <strong>Profil Karyawan</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    <strong>NIK</strong>
                                                </td>
                                                <td width="80%">
                                                    <?php echo $nik;?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    <strong>Nama</strong>
                                                </td>
                                                <td width="80%">
                                                    <?php echo $nama;?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    <strong>Tempat Tanggal Lahir</strong>
                                                </td>
                                                <td width="80%">
                                                    <?php echo $tempat;?>,<?php echo $tanggal;?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>Jenis Kelamin<strong>
                                                </td>
                                                <td width="80%">
                                                    <?php echo $jk;?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>Agama<strong>
                                                </td>
                                                <td width="80%">
                                                    <?php echo $agama;?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>Pedidikan Terakhir<strong>
                                                </td>
                                                <td width="80%">
                                                    <?php echo $pendidikan;?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>Pengalaman Kerja<strong>
                                                </td>
                                                <td width="80%">
                                                    <?php echo $pengalaman;?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>Email<strong>
                                                </td>
                                                <td width="80%">
                                                    <?php echo $email;?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>No Hp<strong>
                                                </td>
                                                <td width="80%">
                                                    <?php echo $no_hp;?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card -->
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->