<?php
	if((isset($_GET['aksi']))&&(isset($_GET['data']))){
		if($_GET['aksi']=='hapus'){
			$kode_departemen = $_GET['data'];
			//hapus departemen
			$sql_dh = "delete from `departemen` where `kode_departemen` = '$kode_departemen'";
		mysqli_query($koneksi,$sql_dh);
		}
	}
?>
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
	<div class="row align-items-center">
		<div class="col-md-6 col-8 align-self-center">
			<h3 class="page-title mb-0 p-0">
				Departemen
			</h3>
			<div class="d-flex align-items-center">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="index.php?include=dashboard">Home</a>
						</li>
						<li class="breadcrumb-item active" aria-current="page">Departemen</li>
					</ol>
				</nav>
			</div>
		</div>
		<div class="col-md-6 col-4 align-self-center">
			<div class="text-right upgrade-btn">
				<a href="index.php?include=tambah_departemen"class="btn btn-success text-white">
					<i class="mr-3 far fa-plus-square"aria-hidden="true"></i>
						Tambah departemen
				</a>
			</div>
		</div>
	</div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<div class="row">
		<!-- column -->
		<div class="col-sm-12">
			<div class="card">
				<div class="row">
					<!-- column -->
					<div class="col-sm-12">
						<div class="card-body">
							<div class="card-header">
								<h3 class="card-title" style="margin-top:5px;">
									<i class="mr-3 far fa-building"aria-hidden="true"></i> Daftar Departemen Perusahaan
								</h3>
							</div> 
							<br><br>
							<div class="card-body">
								<div class="col-md-12">
									<form method="post" action="index.php?include=departemen">
										<div class="row">
											<div class="col-md-4 bottom-10">
												<input type="text" class="form-control" id="kata_kunci" name="katakunci" placeholder="Cari Kode Departemen">
											</div>
											<div class="col-md-5 bottom-10">
												<button type="submit" class="btn btn-primary">
													<i class="mr-3 fas fa-search"aria-hidden="true"></i>&nbsp; Search
												</button>
											</div>
										</div>
										<!-- .row -->
									</form>
								</div>
								<div class="col-sm-12">
									<?php if(!empty($_GET['notif'])){?>
										<?php if($_GET['notif']=="tambahberhasil"){?>
											<div class="alert alert-success" role="alert">
												Data Berhasil Ditambahkan
											</div>
										<?php } 
										else if($_GET['notif']=="editberhasil"){?>
											<div class="alert alert-success" role="alert">
												Data Berhasil Diubah
											</div>
										<?php }?>
									<?php }?>
								</div>
								<table class="table table-bordered">
									<thead>                  
										<tr>
											<th width="5%">NO</th>
											<th width="15%">Id Departemen</th>
											<th width="15%">Nama Departemen</th>
											<th width="15%">Deskripsi</th>
											<th width="13%">
												<center>Aksi</center>
											</th>
										</tr>
									</thead>
									<tbody>
										<?php
											//menampilkan data hobi
											$batas = 3;
											if(!isset($_GET['halaman'])){
												$posisi = 0;
												$halaman = 1;	
											}else{
												$halaman = $_GET['halaman'];
												$posisi = ($halaman-1) * $batas;
											}
											$x = $posisi+1;
											$sql_h = "select `kode_departemen`,`nama_departemen`,`deskripsi` from `departemen`"; 
											if (isset($_POST["katakunci"])){
												$katakunci_departemen = $_POST["katakunci"];
												$_SESSION['katakunci_departemen'] = $katakunci_departemen;
												$sql_h .= " where `kode_departemen` LIKE '%$katakunci_departemen%' ";
											} 
											$sql_h .= "order by `kode_departemen` limit $posisi, $batas";
											$query_h = mysqli_query($koneksi,$sql_h);
											$no=$posisi+1;
											while($data_h = mysqli_fetch_row($query_h)){
												$kode_departemen = $data_h[0];
												$nama_departemen = $data_h[1];
												$deskripsi = $data_h[2];
										?>	
										<tr>
											<td><?php echo $no;?></td>
											<td><?php echo $kode_departemen;?></td>
											<td><?php echo $nama_departemen;?></td>
											<td><?php echo $deskripsi;?></td>
											<td align="center">
												<a href="index.php?include=edit_departemen&data=<?php echo $kode_departemen;?>" class="btn btn-xs btn-info">
													<i class="fas fa-edit"></i> Edit
												</a>
												<a href="javascript:if(confirm('Anda yakin ingin menghapus data <?php echo $nama_departemen; ?>?'))window.location.href = 'index.php?include=departemen&aksi=hapus&data=<?php echo $kode_departemen;?>'" class="btn btn-xs btn-warning">
													<i class="fas fa-trash"></i> Hapus 
												</a> 
											</td>
										</tr>
										<?php
											$no++;
										}?>
										<?php 
											//hitung jumlah semua data 
											$sql_jum = "select `kode_departemen`,`nama_departemen`,`deskripsi` from `departemen`"; 
											if (isset($_SESSION['katakunci_departemen'])){
												$katakunci_departemen = $_SESSION['katakunci_departemen'];
												$sql_jum .= " where `nama_departemen` LIKE '%$katakunci_departemen%'";
											} 
											$sql_jum .= " order by `kode_departemen`";
											$query_jum = mysqli_query($koneksi,$sql_jum);
											$jum_data = mysqli_num_rows($query_jum);
											$jum_halaman = ceil($jum_data/$batas);
										?>
									</tbody>
								</table>
							</div>
							<!-- /.card-body -->
							<div class="card-footer clearfix">
								<span>Showing <?php if($jum_data>0){echo $x;}else {echo 0;}?> to <?php echo $no-1;?> of <?php echo $jum_data;?> entries</span>
								<ul class="pagination pagination-sm m-0 float-right">
									<?php 
										if($jum_halaman==0){
											//tidak ada halaman
										}else if($jum_halaman==1){
											echo "<li class='page-item'><a class='page-link'>1</a></li>";
										}else{
											$sebelum = $halaman-1;
											$setelah = $halaman+1;                  
											if($halaman!=1){
												echo "<li class='page-item'><a class='page-link' href='index.php?include=departemen&halaman=1'>First</a></li>";
												echo "<li class='page-item'><a class='page-link' href='index.php?include=departemen&halaman=$sebelum'>«</a></li>";
											}
											//menampilkan angka halaman
											for($i=1; $i<=$jum_halaman; $i++){
												if($i!=$halaman){
													echo "<li class='page-item'><a class='page-link' href='index.php?include=departemen&halaman=$i'>$i</a></li>";
												}else{
													echo "<li class='page-item'><a class='page-link'>$i</a></li>";
												}
											}
											if($halaman!=$jum_halaman){
												echo "<li class='page-item'><a class='page-link'  href='index.php?include=departemen&halaman=$setelah'>»</a></li>";
												echo "<li class='page-item'><a class='page-link' href='index.php?include=departemen&halaman=$jum_halaman'>Last</a></li>";
											}
										}
									?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Right sidebar -->
	<!-- ============================================================== -->
	<!-- .right-sidebar -->
	<!-- ============================================================== -->
	<!-- End Right sidebar -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->