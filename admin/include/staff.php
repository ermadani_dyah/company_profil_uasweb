<?php 
	if((isset($_GET['aksi']))&&(isset($_GET['data']))){
		if($_GET['aksi']=='hapus'){
			$nik = $_GET['data'];
			$sql_dm = "delete from `karyawan_keuangan` where `nik` = '$nik'";
			mysqli_query($koneksi,$sql_dm);
		}
	}
?>
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">
                Staff Adminstrasi
            </h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.php?include=dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Staff Adminstrasi</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-right upgrade-btn">
                <a href="index.php?include=tambah_staff" class="btn btn-success d-none d-md-inline-block text-white">
                    <i class="mr-3 far fa-plus-square" aria-hidden="true"></i>
                    Tambah Staff Adminstrasi
                </a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- column -->
        <div class="col-sm-12">
            <section class="content">
                <div class="card">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="card-header">
                            <h3 class="card-title" style="margin-top:5px;">
                                <i class="mr-3 far fas  far fa-user""aria-hidden=" true"></i> Data Staff Adminstrasi
                            </h3>
                        </div>
                        <br>
                        <div class="col-md-12">
                            <form method="post" action="index.php?include=staff">
                                <div class="row">
                                    <div class="col-md-4 bottom-10">
                                        <input type="text" class="form-control" id="kata_kunci" name="katakunci" placeholder="Cari NIK/Nama">
                                    </div>
                                    <div class="col-md-5 bottom-10">
                                        <button type="submit" class="btn btn-primary">
                                            <i class="fas fa-search"></i>&nbsp; Search
                                        </button>
                                    </div>
                                </div>
                                <!-- .row -->
                            </form>
                        </div><br><br><br>
                        <div class="col-sm-12">
                            <?php if(!empty($_GET['notif'])){?>
                            <?php if($_GET['notif']=="tambahberhasil"){?>
                            <div class="alert alert-success" role="alert">
                                Data Berhasil Ditambahkan
                            </div>
                            <?php } else if($_GET['notif']=="editberhasil"){?>
                            <div class="alert alert-success" role="alert">
                                Data Berhasil Diubah
                            </div>
                            <?php }?>
                            <?php }?>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="15%">NIK</th>
                                    <th width="20%">Nama</th>
                                    <th width="15%">Jenis Kelamin</th>
                                    <th width="10%">Cabang</th>
                                    <th width="10%">Jabatan</th>
                                    <th width="15%">
                                        <center>Aksi</center>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
									//menampilkan data hobi
									$batas = 3;
									if(!isset($_GET['halaman'])){
										$posisi = 0;
										$halaman = 1;	
									}else{
										$halaman = $_GET['halaman'];
										$posisi = ($halaman-1) * $batas;
									}
									$x = $posisi+1;
									$sql_h = "SELECT `k`.`nik`,`s`.`nama`,`s`.`jenis_kelamin`,`j`.`kode_jabatan`,`c`.`kode_cabang` from `karyawan_keuangan` `s` inner join `karyawan` `k`
											on `s`.`nik` = `k`.`nik` INNER JOIN  `cabang` `c` ON `s`.`kode_cabang`= `c`.`kode_cabang` inner join `jabatan` `j` 
											on `s`.`kode_jabatan` = `j`.`kode_jabatan`"; 
									if (isset($_POST["katakunci"])){
										$katakunci_staff = $_POST["katakunci"];
										$_SESSION['katakunci_staff'] = $katakunci_staff;
										$sql_h .= " where `s`.`nik` LIKE '%$katakunci_staff%' OR `s`.`nama` LIKE '%$katakunci_staff%'";
									} 
									$sql_h .= "order by `nik` limit $posisi, $batas";
									$query_h = mysqli_query($koneksi,$sql_h);
									$no=$posisi+1;
									while($data_h = mysqli_fetch_row($query_h)){
										$nik = $data_h[0];
										$nama = $data_h[1];
										$jenis_kelamin=$data_h[2];
										$kode_jabatan=$data_h[3];
										$kode_cabang=$data_h[4];
								?>
                                <tr>
                                    <td><?php echo $no;?></td>
                                    <td><?php echo $nik;?></td>
                                    <td><?php echo $nama;?></td>
                                    <td><?php echo $jenis_kelamin;?></td>
                                    <td><?php echo $kode_cabang;?></td>
                                    <td><?php echo $kode_jabatan;?></td>
                                    <td>
                                        <a href="index.php?include=edit_staff&data=<?php echo $nik;?>" class="btn btn-xs btn-info">
                                            <i class="fas fa-edit"></i> Edit
                                        </a>
                                        <a href="javascript:if(confirm('Anda yakin ingin menghapus data <?php echo $nama; ?>?'))window.location.href = 'index.php?include=staff&aksi=hapus&data=<?php echo $nik;?>'" class="btn btn-xs btn-warning">
                                            <i class="fas fa-trash"></i> Hapus
                                        </a>
                                    </td>
                                </tr>
                                <?php
									$no++;
								}?>
                                <?php 
									//hitung jumlah semua data 
									$sql_jum = "SELECT `k`.`nik`,`s`.`nama`,`s`.`jenis_kelamin`,`j`.`kode_jabatan`,`c`.`kode_cabang` from `karyawan_keuangan` `s` inner join `karyawan` `k`
												on `s`.`nik` = `k`.`nik` INNER JOIN  `cabang` `c` ON `s`.`kode_cabang`= `c`.`kode_cabang` inner join `jabatan` `j` 
												on `s`.`kode_jabatan` = `j`.`kode_jabatan` "; 
									if (isset($_SESSION['katakunci_staff'])){
										$katakunci_staff = $_SESSION['katakunci_staff'];
										$sql_jum .= " where `s`.`nik` LIKE '%$katakunci_staff%'
													OR `s`.`nama` LIKE '%$katakunci_staff%'";
									} 
									$sql_jum .= "order by `nik`";
									$query_jum = mysqli_query($koneksi,$sql_jum);
									$jum_data = mysqli_num_rows($query_jum);
									$jum_halaman = ceil($jum_data/$batas);
								?>
                            </tbody>
                        </table>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix">
                            <span>Showing <?php if($jum_data>0){echo $x;}else {echo 0;}?> to <?php echo $no-1;?> of <?php echo $jum_data;?> entries</span>
                            <ul class="pagination pagination-sm m-0 float-right">
                                <?php 
									if($jum_halaman==0){
										//tidak ada halaman
									}else if($jum_halaman==1){
										echo "<li class='page-item'><a class='page-link'>1</a></li>";
									}else{
										$sebelum = $halaman-1;
										$setelah = $halaman+1;                  
										if($halaman!=1){
											echo "<li class='page-item'><a class='page-link' href='index.php?include=staff&halaman=1'>First</a></li>";
											echo "<li class='page-item'><a class='page-link' href='index.php?include=staff&halaman=$sebelum'>«</a></li>";
										}
										//menampilkan angka halaman
										for($i=1; $i<=$jum_halaman; $i++){
											if($i!=$halaman){
												echo "<li class='page-item'><a class='page-link' href='index.php?include=staff&halaman=$i'>$i</a></li>";
											}else{
												echo "<li class='page-item'><a class='page-link'>$i</a></li>";
											}
										}
										if($halaman!=$jum_halaman){
											echo "<li class='page-item'><a class='page-link'  href='index.php?include=staff&halaman=$setelah'>»</a></li>";
											echo "<li class='page-item'><a class='page-link' href='index.php?include=staff&halaman=$jum_halaman'>Last</a></li>";
										}
									}
								?>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /.card -->
            </section>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->