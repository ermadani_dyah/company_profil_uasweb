<?php 
		$id_admin = $_SESSION['id_admin'];
			//get profil
		$sql = "select `nama`,`nama_lengkap`, `email`,`no_hp` from `admin` where `id_admin`='$id_admin'";
		//echo $sql;
		$query = mysqli_query($koneksi, $sql);
		while($data = mysqli_fetch_row($query)){
				$nama = $data[0];
				$nama_lengkap= $data[1];
				$email = $data[2];
				$hp = $data[3];
	}
?>
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Profile</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
							<a href="index.php?include=dashboard">Home</a>
						</li>
                        <li class="breadcrumb-item active" aria-current="page">Profile</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-right upgrade-btn">
                <a href="index.php?include=edit_profil" class="btn btn-success d-none d-md-inline-block text-white">
                    <i class="mr-3 fas fa-edit" aria-hidden="true"></i>
                    Edit Profil
                </a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="col-sm-12">
        <?php if(!empty($_GET['notif'])){
							if($_GET['notif']=="editberhasil"){?>
        <div class="alert alert-success" role="alert">Data Berhasil Diubah</div>
        <?php }?>
        <?php }?>
    </div>
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card">
                <div class="card-body profile-card">
                    <center class="m-t-30"> <img src="assets/images/users/5.jpg" class="rounded-circle" width="150" />
                        <h4 class="card-title m-t-10"><?php echo $nama;?></h4>
                        <h6 class="card-subtitle"><?php echo $hp;?></h6>
                    </center>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <div class="card-body">
                    <form class="form-horizontal form-material">
                        <div class="form-group">
                            <label class="col-md-12 mb-0">Full Name</label>
                            <div class="col-md-12">
                                <input type="text" placeholder="<?php echo $nama_lengkap;?>" class="form-control pl-0 form-control-line">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="example-email" class="col-md-12">Departemen</label>
                            <div class="col-md-12">
                                <input type="text" placeholder="Keuangan dan Administrasi" class="form-control pl-0 form-control-line">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12 mb-0">Email</label>
                            <div class="col-md-12">
                                <input type="email" placeholder="<?php echo $email;?>" class="form-control pl-0 form-control-line" name="example-email" id="example-email">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->