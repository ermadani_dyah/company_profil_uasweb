<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">
                <i class="mr-3 fas fa-plus" aria-hidden="true"></i>
                Tambah Staff Administrasi
            </h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.php?include=dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Staff Administrasi</li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah Staff Administrasi</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-right upgrade-btn">
                <a href="index.php?include=staff" class="btn btn-sm btn-warning d-none d-md-inline-block text-white">
                    <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- column -->
        <div class="col-sm-12">
            <div class="card">
                <div class="row">
                    <!-- column -->
                    <div class="col-sm-12">
                        <div class="card-body">
                            <section class="content">
                                <div class="card card-info">
                                    <div class="card-header">
                                        <h3 class="card-title" style="margin-top:5px;">
                                            <i class="far fa-list-alt"></i> Form Tambah Data User
                                        </h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <br>
                                    <div class="col-sm-10">
                                        <?php if((!empty($_GET['notif']))&&(!empty($_GET['jenis']))){?>
                                        <?php if($_GET['notif']=="tambahkosong"){?>
                                        <div class="alert alert-danger" role="alert">Maaf data
                                            <?php echo $_GET['jenis'];?> wajib di isi
                                        </div>
                                        <?php }?>
                                        <?php }?>
                                    </div>
                                    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="index.php?include=konfirmasi_tambah_staff">
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label for="hobi" class="col-sm-3 col-form-label">NIK</label>
                                                <div class="col-sm-7">
                                                    <select class="form-control" id="nik" name="nik">
                                                        <option value=""></option>
                                                        <?php
															$sql_cbox_nik = "SELECT `NIK` FROM `karyawan` ORDER BY `NIK`";
															$query_cbox_nik = mysqli_query($koneksi,$sql_cbox_nik);
															while($data_cbox_nik = mysqli_fetch_row($query_cbox_nik)){
																$cbox_nik = $data_cbox_nik[0];?>
                                                        <option value="<?php echo $cbox_nik;?>" <?php if(!empty($_SESSION['nik'])){if($cbox_nik==$_SESSION['nik']){ ?> selected="selected" <?php }}?>>
                                                            <?php echo $cbox_nik;?>
                                                        </option>
                                                        <?php
															} 
														?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="hobi" class="col-sm-3 col-form-label">Nama</label>
                                                <div class="col-sm-7">
                                                    <input type="text" name="nama" class="form-control" id="nama" value="<?php if(!empty($_SESSION['nama'])){ echo $_SESSION['nama'];} ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="hobi" class="col-sm-3 col-form-label">Jenis Kelamin</label>
                                                <div class="col-sm-7">
                                                    <select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
                                                        <option value="0">- Pilih Jenis Kelamin -</option>
                                                        <option value="Perempuan">Perempuan</option>
                                                        <option value="Laki Laki">Laki- Laki</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="hobi" class="col-sm-3 col-form-label">Cabang Perusahaan</label>
                                                <div class="col-sm-7">
                                                    <select class="form-control" id="cabang" name="cabang">
                                                        <option value="0">-Pilih Cabang-</option>
                                                        <?php
															$sql_j ="select `kode_cabang`,`nama_cabang` from `cabang` order by `kode_cabang`";
															$query_j = mysqli_query($koneksi,$sql_j);
															while($data_j = mysqli_fetch_row($query_j)){
																$kode_cabang = $data_j[0];
																$nama_cabang = $data_j[1];
														?>
                                                        <option value="<?php echo $kode_cabang;?>" <?php if(!empty($_SESSION['kode_cabang'])){if($kode_cabang==$_SESSION['kode_cabang']){?> selected="selected" <?php }}?>>
                                                            <?php echo "$kode_cabang - $nama_cabang";?>
                                                            <?php }?>
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="password" class="col-sm-3 col-form-label">Jenis Jabatan</label>
                                                <div class="col-sm-7">
                                                    <select class="form-control" id="jabatan" name="jabatan">
                                                        <option value="0">-Pilih Jabatan-</option>
                                                        <?php
															$sql_j ="select `kode_jabatan`,`nama_jabatan` from `jabatan` order by `kode_jabatan`";
															$query_j = mysqli_query($koneksi,$sql_j);
															while($data_j = mysqli_fetch_row($query_j)){
																$kode_jabatan = $data_j[0];
																$nama_jabatan = $data_j[1];
														 ?>
                                                        <option value="<?php echo $kode_jabatan;?>" <?php if(!empty($_SESSION['kode_jabatan'])){if($kode_jabatan==$_SESSION['kode_jabatan']){?> selected="selected" <?php }}?>>
                                                            <?php echo "$kode_jabatan - $nama_jabatan";?>
                                                            <?php }?>
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-info float-right">
                                                <i class="mr-3 fas fa-plus" aria-hidden="true"></i>Tambah
                                            </button>
                                        </div>
                                        <!-- /.card-footer -->
                                    </form>
                                </div>
                                <!-- /.card -->
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->