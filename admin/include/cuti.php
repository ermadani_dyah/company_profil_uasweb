<?php
	if((isset($_GET['aksi']))&&(isset($_GET['n']))&&(isset($_GET['b']))&&(isset($_GET['t']))&&(isset($_GET['j']))){
		if($_GET['aksi']=='hapus'){
			$n = $_GET['n'];
			$b = $_GET['b'];
			$t = $_GET['t'];
			$j = $_GET['j'];
			//hapus cabang
			$sql_dh = "DELETE FROM `cuti` WHERE `nik`='$n' AND `bulan`='$b' AND `tahun`='$t' AND `jml_cut`='$j' ";
		mysqli_query($koneksi,$sql_dh);
		}
	}
?>
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
	<div class="row align-items-center">
		<div class="col-md-6 col-8 align-self-center">
			<h3 class="page-title mb-0 p-0">
				Cuti
			</h3>
			<div class="d-flex align-items-center">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="index.php?include=dashboard">Home</a>
						</li>
						<li class="breadcrumb-item active" aria-current="page">Cuti</li>
					</ol>
				</nav>
			</div>
		</div>
		<div class="col-md-6 col-4 align-self-center">
			<div class="text-right upgrade-btn">
				<a href="index.php?include=tambah_cuti"class="btn btn-success d-none d-md-inline-block text-white">
					<i class="mr-3 far fa-plus-square"aria-hidden="true"></i>
					Tambah Data Cuti
				</a>
			</div>
		</div>
	</div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->
	<div class="row">
    <!-- column -->
		<div class="col-sm-12">
			<div class="card">
					<div class="row">
					<!-- column -->
						<div class="col-sm-12">
							<div class="card">
								<div class="card-body">
									<div class="card-header">
										<h3 class="card-title" style="margin-top:5px;">
											<i class="mr-3 fas fa-street-view"aria-hidden="true"></i> Daftar Data Cuti
										</h3>
									</div> 
									<br>
									<div class="card-body">
										<div class="col-md-12">
											<form method="post" action="index.php?include=cuti">
												<div class="row">
													<div class="col-md-4 bottom-10">
														<input type="text" class="form-control" id="katakunci_cuti" name="katakunci_cuti" placeholder="Cari NIK / Bulan / Tahun" value="<?php  if(isset($_POST["katakunci_cuti"])){$_SESSION['katakunci_cuti'] = $_POST["katakunci_cuti"];}if(isset($_SESSION['katakunci_cuti'])){echo $_SESSION['katakunci_cuti'];}?>">
													</div>
													<div class="col-md-5 bottom-10">
														<button type="submit" class="btn btn-primary">
															<i class="mr-3 fas fa-search"aria-hidden="true"></i>&nbsp; Search
														</button>
													</div>
												</div>
												<!-- .row -->
											</form>
										</div>
										<div class="col-sm-12">
											<?php if(!empty($_GET['notif'])){?>
												<?php if($_GET['notif']=="tambahberhasil"){?>
													<div class="alert alert-success" role="alert">
														Data Berhasil Ditambahkan
													</div>
												<?php } else if($_GET['notif']=="editberhasil"){?>
													<div class="alert alert-success" role="alert">
														Data Berhasil Diubah
													</div>
												<?php }?>
											<?php }?>
										</div>
										<table class="table table-bordered">
											<thead>                  
												<tr>
													<th width="5%">No</th>
													<th width="15%">NIK</th>
													<th width="20%">Bulan</th>
													<th width="10%">Tahun</th>
													<th width="20%">Jumlah Cuti</th>
													<th width="30%">
														<center>Aksi</center>
													</th>
												</tr>
											</thead>
											<tbody>
												<?php
													// menetapkan batas, posisi, halaman
													$batas = 3;
													if(!isset($_GET['halaman'])){
														$posisi = 0;
														$halaman = 1;
													}else{
														$halaman = $_GET['halaman'];
														$posisi = ($halaman-1) * $batas;
													}
													$x = $posisi+1;
													//menampilkan data hobi
													$sql_h = "SELECT `nik`, `bulan`, `tahun`, `jml_cut` FROM `cuti` "; 
													if (isset($_POST["katakunci_cuti"])){
														$katakunci_cuti = $_POST["katakunci_cuti"];
														$_SESSION['katakunci_cuti'] = $katakunci_cuti;
													} 
													if(isset($_SESSION['katakunci_cuti'])){
														$katakunci_cuti = $_SESSION['katakunci_cuti'];
														$sql_h .= " WHERE `nik` LIKE '%$katakunci_cuti%' 
																	OR `bulan` LIKE '%$katakunci_cuti%' 
																	OR `tahun` LIKE '%$katakunci_cuti%' ";
													} 
													$sql_h .= "ORDER BY `nik` limit $posisi, $batas";
													$query_h = mysqli_query($koneksi,$sql_h);
													$no=$posisi+1;
													while($data_h = mysqli_fetch_row($query_h)){
														$nik = $data_h[0];
														$bulan = $data_h[1];
														$tahun = $data_h[2];
														$jumlah_cuti = $data_h[3];
												?>
												<tr>
													<td><?php echo $no;?></td>
													<td><?php echo $nik;?></td>
													<td><?php echo $bulan;?></td>
													<td><?php echo $tahun;?></td>
													<td><?php echo $jumlah_cuti;?></td>
													<td align="center">
														<a href="index.php?include=edit_cuti&data=<?php echo $nik;?>" class="btn btn-xs btn-info">
															<i class="fas fa-edit"></i> Edit
														</a>
														<a href="javascript:if(confirm('Anda yakin ingin menghapus data <?php echo $bulan; ?>?'))window.location.href = 'index.php?include=cuti&aksi=hapus&data=<?php echo $nik;?>'" class="btn btn-xs btn-warning">
															<i class="fas fa-trash"></i> Hapus 
														</a> 
													</td>
												</tr>
												<?php
													$no++;
												}?>
											</tbody>
										</table>
									</div>
									<!-- /.card-body -->
									<?php
										//hitung jumlah semua data 
										$sql_jum = "SELECT * FROM `cuti` "; 
										if (isset($_SESSION["katakunci_cuti"])){
											$katakunci_jabatan = $_SESSION["katakunci_cuti"];
											$sql_jum .= "WHERE `nik` LIKE '%$katakunci_cuti%' 
														OR `bulan` LIKE '%$katakunci_cuti%' 
														OR `tahun` LIKE '%$katakunci_cuti%' ";
										}
										$query_jum = mysqli_query($koneksi,$sql_jum);
										$jum_data = mysqli_num_rows($query_jum);
										$jum_halaman = ceil($jum_data/$batas);
									?>
									<div class="card-footer clearfix">
										<span>Showing <?php if($jum_data>0){echo $x;}else {echo 0;}?> to <?php echo $no-1;?> of <?php echo $jum_data;?> entries</span>
										<ul class="pagination pagination-sm m-0 float-right">
											<?php 
												if($jum_halaman==0){
												//tidak ada halaman
												}else if($jum_halaman==1){
													echo "<li class='page-item'><a class='page-link'>1</a></li>";
												}else{
													$sebelum = $halaman-1;
													$setelah = $halaman+1;                  
													if($halaman!=1){
														echo "<li class='page-item'><a class='page-link' href='index.php?include=cuti&halaman=1'>First</a></li>";
														echo "<li class='page-item'><a class='page-link' href='index.php?include=cuti&halaman=$sebelum'>«</a></li>";
													}
													//menampilkan angka halaman
													for($i=1; $i<=$jum_halaman; $i++){
														if($i!=$halaman){
															echo "<li class='page-item'><a class='page-link' href='index.php?include=cuti&halaman=$i'>$i</a></li>";
														}else{
															echo "<li class='page-item'><a class='page-link'>$i</a></li>";
														}
													}
													if($halaman!=$jum_halaman){
														echo "<li class='page-item'><a class='page-link'  href='index.php?include=cuti&halaman=$setelah'>»</a></li>";
														echo "<li class='page-item'><a class='page-link' href='index.php?include=cuti&halaman=$jum_halaman'>Last</a></li>";
													}
												}
											?>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Right sidebar -->
<!-- ============================================================== -->
<!-- .right-sidebar -->
<!-- ============================================================== -->
<!-- End Right sidebar -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->														