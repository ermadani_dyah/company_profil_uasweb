<?php
	if(isset($_GET['data'])){
		$kode_lamaran = $_GET['data'];
		$_SESSION['kode_lamaran']=$kode_lamaran;
		//get data mahasiswa
		$sql_m ="SELECT `l`.`kode_lamaran`,`l`.`syarat`,`c`.`kode_cabang`,`l`.`bagian` from `tabel_lamaran` `l` INNER JOIN 
                `cabang` `c` on `l`.`kode_cabang` = `c`.`kode_cabang` where `kode_lamaran`='$kode_lamaran'"; 
		$query_m = mysqli_query($koneksi,$sql_m);
		while($data_m = mysqli_fetch_row($query_m)){
			$kode_lamaran = $data_m[0];
            $syarat = $data_m[1];
            $kode_cabang = $data_m[2];
            $bagian = $data_m[3];
		}
	}
?>
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">
                <i class="mr-3 fas fa-pencil-alt" aria-hidden="true"></i>
                Edit Daftar Berita Lamaran 
            </h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.php?include=dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Lamaran Pekerjaan</li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Daftar Berita Lamaran</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-right upgrade-btn">
                <a href="index.php?include=lamaran" class="btn btn-sm btn-warning d-none d-md-inline-block text-white">
                    <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- column -->
        <div class="col-sm-12">
            <div class="card">
                <div class="row">
                    <!-- column -->
                    <div class="col-sm-12">
                        <div class="card-body">
                            <section class="content">
                                <div class="card card-info">
                                    <div class="card-header">
                                        <h3 class="card-title" style="margin-top:5px;">
                                            <i class="far fa-list-alt"></i> Form Edit Data Karyawan
                                        </h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <br>
                                    <div class="col-sm-10">
                                        <?php if((!empty($_GET['notif']))&&(!empty($_GET['jenis']))){?>
                                        <?php if($_GET['notif']=="editkosong"){?>
                                        <div class="alert alert-danger" role="alert">
                                            Maaf data
                                            <?php echo $_GET['jenis'];?> wajib di isi
                                        </div>
                                        <?php }?>
                                        <?php }?>
                                    </div>
                                    <form class="form-horizontal" method="post" action="index.php?include=konfirmasi_edit_lamaran">
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label for="nama" class="col-sm-3 col-form-label">Kode Lamaran</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="kode_lamaran" name="kode_lamaran" value="<?php echo $kode_lamaran;?>" readonly="readonly">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="hobi" class="col-sm-3 col-form-label">Syarat</label>
                                                <div class="col-sm-7">
                                                    <textarea class="form-control" id="syarat" name="syarat" cols="30" rows="7"><?php echo $syarat;?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="nama" class="col-sm-3 col-form-label">Bagian</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="bagian" name="bagian" value="<?php echo $bagian;?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="jurusan" class="col-sm-3 col-form-label">Cabang</label>
                                                <div class="col-sm-7">
                                                    <select class="form-control" id="cabang" name="cabang">
                                                        <option value="0">- Pilih cabang-</option>
                                                        <?php
															$sql_j ="select `kode_cabang`,`nama_cabang` from `cabang` order by `kode_cabang`";
															$query_j = mysqli_query($koneksi,$sql_j);
															while($data_j = mysqli_fetch_row($query_j)){
															$kode_cabang = $data_j[0];
															$nama_cabang = $data_j[1];
														?>
                                                        <option value="<?php echo $kode_cabang;?>" <?php if($kode_cabang==$kode_cabang){?> selected="selected" <?php }?>>
                                                            <?php echo "$kode_cabang - $nama_cabang";?>
                                                            <?php }?>
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-info float-right">
                                                <i class="mr-3  far fa-save" aria-hidden="true"></i>Simpan
                                            </button>
                                        </div>
                                        <!-- /.card-footer -->
                                    </form>
                                </div>
                                <!-- /.card -->
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->