<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
	<div class="row align-items-center">
		<div class="col-md-6 col-8 align-self-center">
			<h3 class="page-title mb-0 p-0">Dashboard</h3>
			<div class="d-flex align-items-center">
				<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<a href="index.php?include=dashboard">Home</a>
					</li>
					<li class="breadcrumb-item active" aria-current="page">Dashboard</li>
				</ol>
				</nav>
			</div>
		</div>
	</div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
	<!-- ============================================================== -->
	<!-- Sales chart -->
	<!-- ============================================================== -->
	<div class="row">
		<!-- Column -->
		<div class="col-sm-6">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">Saham Perhari</h4>
					<div class="text-right">
						<h2 class="font-light m-b-0"><i class="ti-arrow-up text-success"></i> $120 </h2>
						<span class="text-muted">Kenaikan</span>
					</div>
					<span class="text-success">80%</span>
					<div class="progress">
						<div class="progress-bar bg-success" role="progressbar" style="width: 80%; height: 6px;" aria-valuenow="25" aria-valuemin="0"aria-valuemax="100"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<div class="col-sm-6">
			<div class="card">
				<div class="card-body">
					<h4 class="card-title">Saham PerMinggu</h4>
					<div class="text-right">
						<h2 class="font-light m-b-0"><i class="ti-arrow-up text-info"></i> $5,000 </h2>
						<span class="text-muted">Kenaikan</span>
					</div>
					<span class="text-info">30%</span>
					<div class="progress">
						<div class="progress-bar bg-info" role="progressbar" style="width: 30%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
				</div>
			</div>
		</div>
		<!-- Column --></div>
	<!-- ============================================================== -->
	<!-- Sales chart -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Table -->
	<!-- ============================================================== -->
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-body">
					<div class="row align-items-center">
						<h4 class="card-title col-md-10 mb-md-0 mb-3">Anggota Kelompok:</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- Table -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Recent blogss -->
	<!-- ============================================================== -->
	<div class="row justify-content-center">
		<!-- Column -->
		<div class="col-lg-4 col-md-6">
			<div class="card">
				<img class="card-img-top img-responsive" src="assets/images/big/fajar.jpg" alt="Card">
				<div class="card-body">
					<h3 class="font-normal">ISNANDAR FAJAR P</h3>
					<p>
						<i class="mr-3 fas fa-id-badge"aria-hidden="true"></i>
						193140914111026
					</p>
					<p>
						<i class="mr-3 fab fa-whatsapp"aria-hidden="true"></i>
						085814718596
					</p>
					<p>
						<i class="mr-3 fas fa-envelope"aria-hidden="true"></i>
						isnandar.1471@gmail.com
					</p>
				</div>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<div class="col-lg-4 col-md-6">
			<div class="card">
				<img class="card-img-top img-responsive" src="assets/images/big/ucup.jpg" alt="Card">
				<div class="card-body">
					<h3 class="font-normal">MUHAMMAD YUSUF A</h3>
					<p>
						<i class="mr-3 fas fa-id-badge"aria-hidden="true"></i>
						193140914111013
					</p>
					<p>
						<i class="mr-3 fab fa-whatsapp"aria-hidden="true"></i>
						088217981923
					</p>
					<p>
						<i class="mr-3 fas fa-envelope"aria-hidden="true"></i>
						Muhamadyusufardianto@gmail.com
					</p>
				</div>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<div class="col-lg-4 col-md-6">
			<div class="card">
				<img class="card-img-top img-responsive" src="assets/images/big/dd2.jpg" alt="Card">
				<div class="card-body">
					<h3 class="font-normal">ERMADANI DYAH R</h3>
					<p>
						<i class="mr-3 fas fa-id-badge"aria-hidden="true"></i>
						193140914111016
					</p>
					<p>
						<i class="mr-3 fab fa-whatsapp"aria-hidden="true"></i>
						081555442712
					</p>
					<p>
						<i class="mr-3 fas fa-envelope"aria-hidden="true"></i>
						ermadani.dyah09@gmail.com
					</p>
				</div>
			</div>
		</div>
		<!-- Column -->
	</div>
	<!-- ============================================================== -->
	<!-- Recent blogss -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->