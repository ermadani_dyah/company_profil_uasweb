<?php
	if((isset($_GET['aksi']))&&(isset($_GET['data']))){
		if($_GET['aksi']=='hapus'){
			$kode_cabang = $_GET['data'];
			//hapus cabang
			$sql_dh = "delete from `cabang` where `kode_cabang` = '$kode_cabang'";
		mysqli_query($koneksi,$sql_dh);
		}
	}
?>
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
	<div class="row align-items-center">
		<div class="col-md-6 col-8 align-self-center">
			<h3 class="page-title mb-0 p-0">
				Cabang
			</h3>
			<div class="d-flex align-items-center">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="index.php?include=dashboard">Home</a>
						</li>
						<li class="breadcrumb-item active" aria-current="page">Cabang</li>
					</ol>
				</nav>
			</div>
		</div>
		<div class="col-md-6 col-4 align-self-center">
			<div class="text-right upgrade-btn">
				<a href="index.php?include=tambah_cabang"class="btn btn-success text-white">
					<i class="mr-3 far fa-plus-square"aria-hidden="true"></i>
						Tambah Cabang
				</a>
			</div>
		</div>
	</div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<div class="row">
		<!-- column -->
		<div class="col-sm-12">
			<div class="card">
				<div class="row">
					<!-- column -->
					<div class="col-sm-12">
						<div class="card-body">
							<div class="card-header">
								<h3 class="card-title" style="margin-top:5px;">
									<i class="mr-3 far fa-building"aria-hidden="true"></i> Daftar Cabang Perusahaan
								</h3>
							</div> 
							<br><br>
							<div class="card-body">
								<div class="col-md-12">
									<form method="post" action="index.php?include=cabang">
										<div class="row">
											<div class="col-md-4 bottom-10">
												<input type="text" class="form-control" id="kata_kunci" name="katakunci" placeholder="Cari Kode Cabang / Nama Cabang"
												value="<?php  
															if(isset($_POST["katakunci"])){
																$_SESSION['katakunci_cabang'] = $_POST["katakunci"];
															}if(isset($_SESSION['katakunci_cabang'])){
																echo $_SESSION['katakunci_cabang'];
															}
														?>"
												>
											</div>
											<div class="col-md-5 bottom-10">
												<button type="submit" class="btn btn-primary">
													<i class="mr-3 fas fa-search"aria-hidden="true"></i>&nbsp; Search
												</button>
											</div>
										</div>
										<!-- .row -->
									</form>
								</div>
								<div class="col-sm-12">
									<?php if(!empty($_GET['notif'])){?>
										<?php if($_GET['notif']=="tambahberhasil"){?>
											<div class="alert alert-success" role="alert">
												Data Berhasil Ditambahkan
											</div>
										<?php } 
										else if($_GET['notif']=="editberhasil"){?>
											<div class="alert alert-success" role="alert">
												Data Berhasil Diubah
											</div>
										<?php }?>
									<?php }?>
								</div>
								<table class="table table-bordered">
									<thead>                  
										<tr>
											<th width="5%">NO</th>
											<th width="15%">Kode Cabang</th>
											<th width="15%">Nama Cabang</th>
											<th width="20%">Alamat</th>
											<th width="15%">Email</th>
											<th width="15%">No. HP</th>
											<th width="15%">
												<center>Aksi</center>
											</th>
										</tr>
									</thead>
									<tbody>
										<?php
											//menampilkan data hobi
											$batas = 3;
											if(!isset($_GET['halaman'])){
												$posisi = 0;
												$halaman = 1;	
											}else{
												$halaman = $_GET['halaman'];
												$posisi = ($halaman-1) * $batas;
											}
											$x = $posisi+1;
											$sql_h = "SELECT `kode_cabang`, `nama_cabang`, `alamat`, `email`, `no_hp` FROM `cabang` "; 
											if (isset($_POST["katakunci"])){	
												$_SESSION['katakunci_cabang'] =$_POST["katakunci"];
											} 
											if (isset($_SESSION['katakunci_cabang'])){
												$katakunci_cabang = $_SESSION['katakunci_cabang'];
												$sql_h .= " where `kode_cabang` LIKE '%$katakunci_cabang%' ";
												$sql_h .= " OR `nama_cabang` LIKE '%$katakunci_cabang%' ";
											} 
											$sql_h .= "order by `kode_cabang` limit $posisi, $batas";
											$query_h = mysqli_query($koneksi,$sql_h);
											$no=$posisi+1;
											while($data_h = mysqli_fetch_row($query_h)){
												$kode_cabang = $data_h[0];
												$nama_cabang = $data_h[1];
												$alamat      = $data_h[2];
												$email       = $data_h[3];
												$noHp        = $data_h[4];
										?>	
										<tr>
											<td><?php echo $no;?></td>
											<td><?php echo $kode_cabang;?></td>
											<td><?php echo $nama_cabang;?></td>
											<td><?= "$alamat"; ?></td>
											<td><?= "$email"; ?></td>
											<td><?= "$noHp"; ?></td>
											<td align="center">
												<a href="index.php?include=edit_cabang&data=<?php echo $kode_cabang;?>" class="btn btn-xs btn-info">
													<i class="fas fa-edit"></i> Edit
												</a>
												<a href="javascript:if(confirm('Anda yakin ingin menghapus data <?php echo $nama_cabang; ?>?'))window.location.href = 'index.php?include=cabang&aksi=hapus&data=<?php echo $kode_cabang;?>'" class="btn btn-xs btn-warning">
													<i class="fas fa-trash"></i> Hapus 
												</a> 
											</td>
										</tr>
										<?php
											$no++;
										}?>
										<?php 
											//hitung jumlah semua data 
											$sql_jum = "SELECT `kode_cabang`, `nama_cabang`, `alamat`, `email`, `no_hp` FROM `cabang` "; 
											if (isset($_SESSION['katakunci_cabang'])){
												$katakunci_cabang = $_SESSION['katakunci_cabang'];
												$sql_jum .= " where `kode_cabang` LIKE '%$katakunci_cabang%'";
												$sql_jum .= " or `nama_cabang` LIKE '%$katakunci_cabang%'";
											} 
											$sql_jum .= " order by `kode_cabang`";
											$query_jum = mysqli_query($koneksi,$sql_jum);
											$jum_data = mysqli_num_rows($query_jum);
											$jum_halaman = ceil($jum_data/$batas);
										?>
									</tbody>
								</table>
							</div>
							<!-- /.card-body -->
							<div class="card-footer clearfix">
								<span>Showing <?php if($jum_data>0){echo $x;}else {echo 0;}?> to <?php echo $no-1;?> of <?php echo $jum_data;?> entries</span>
								<ul class="pagination pagination-sm m-0 float-right">
									<?php 
										if($jum_halaman==0){
											//tidak ada halaman
										}else if($jum_halaman==1){
											echo "<li class='page-item'><a class='page-link'>1</a></li>";
										}else{
											$sebelum = $halaman-1;
											$setelah = $halaman+1;                  
											if($halaman!=1){
												echo "<li class='page-item'><a class='page-link' href='index.php?include=cabang&halaman=1'>First</a></li>";
												echo "<li class='page-item'><a class='page-link' href='index.php?include=cabang&halaman=$sebelum'>«</a></li>";
											}
											//menampilkan angka halaman
											for($i=1; $i<=$jum_halaman; $i++){
												if($i!=$halaman){
													echo "<li class='page-item'><a class='page-link' href='index.php?include=cabang&halaman=$i'>$i</a></li>";
												}else{
													echo "<li class='page-item'><a class='page-link'>$i</a></li>";
												}
											}
											if($halaman!=$jum_halaman){
												echo "<li class='page-item'><a class='page-link'  href='index.php?include=cabang&halaman=$setelah'>»</a></li>";
												echo "<li class='page-item'><a class='page-link' href='index.php?include=cabang&halaman=$jum_halaman'>Last</a></li>";
											}
										}
									?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Right sidebar -->
	<!-- ============================================================== -->
	<!-- .right-sidebar -->
	<!-- ============================================================== -->
	<!-- End Right sidebar -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->