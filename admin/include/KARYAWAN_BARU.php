<?php
	if((isset($_GET['aksi']))&&(isset($_GET['data']))){
		if($_GET['aksi']=='hapus'){
			$nik = $_GET['data'];
			//hapus karyawan baru
			$sql_dh = "delete from `karyawan_baru` where `nik` = '$nik'";
		mysqli_query($koneksi,$sql_dh);
		}
	}
?>
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
	<div class="row align-items-center">
		<div class="col-md-6 col-8 align-self-center">
			<h3 class="page-title mb-0 p-0">
				Karyawan Baru
			</h3>
			<div class="d-flex align-items-center">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item">
							<a href="index.php?include=dashboard">Home</a>
						</li>
						<li class="breadcrumb-item active" aria-current="page">Karyawan Baru</li>
					</ol>
				</nav>
			</div>
		</div>
		<!-- <div class="col-md-6 col-4 align-self-center">
			<div class="text-right upgrade-btn">
				<a href="index.php?include=tambah_cabang"class="btn btn-success text-white">
					<i class="mr-3 far fa-plus-square"aria-hidden="true"></i>
						Tambah Cabang
				</a>
			</div>
		</div> -->
	</div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<div class="row">
		<!-- column -->
		<div class="col-sm-12">
			<div class="card">
				<div class="row">
					<!-- column -->
					<div class="col-sm-12">
						<div class="card-body">
							<div class="card-header">
								<h3 class="card-title" style="margin-top:5px;">
									<i class="mr-3 fas fa-clipboard-list"aria-hidden="true"></i> Daftar Karyawan Baru
								</h3>
							</div> 
							<br><br>
							<div class="card-body">
								<div class="col-md-12">
									<?php 
										//menampilkan data hobi
										$batas = 5;
										if(!isset($_GET['halaman'])){
											$posisi = 0;
											$halaman = 1;	
										}else{
											$halaman = $_GET['halaman'];
											$posisi = ($halaman-1) * $batas;
										}
										$x = $posisi+1;
										$sql_h = "SELECT * from `karyawan_baru`"; 
										if (isset($_POST["katakunci"])){
											$kataKunci_karyawanBaru = $_POST["katakunci"];
											$_SESSION['kataKunci_karyawanBaru'] = $kataKunci_karyawanBaru;
											$sql_h .= " WHERE `nik` LIKE '%$kataKunci_karyawanBaru%' ";
											$sql_h .= " OR `nama` LIKE '%$kataKunci_karyawanBaru%' ";
										} 
										$sql_h .= "order by `nik` limit $posisi, $batas";
										$query_h = mysqli_query($koneksi,$sql_h);
										$no=$posisi+1; 
									?>
									<form method="post" action="index.php?include=karyawan_baru">
										<div class="row">
											<div class="col-md-4 bottom-10">
												<input type="text" class="form-control" id="kata_kunci" name="katakunci" placeholder="Cari Karyawan Baru"
													value="<?php  
															if(isset($_POST["kataKunci_karyawanBaru"])){
																$_SESSION['kataKunci_karyawanBaru'] = $_POST["kataKunci_karyawanBaru"];
															}if(isset($_SESSION['kataKunci_karyawanBaru'])){
																echo $_SESSION['kataKunci_karyawanBaru'];
															}?>"
												>
											</div>
											<div class="col-md-5 bottom-10">
												<button type="submit" class="btn btn-primary">
													<i class="mr-3 fas fa-search"aria-hidden="true"></i>&nbsp; Search
												</button>
											</div>
										</div>
										<!-- .row -->
									</form>
								</div>
								<!-- <div class="col-sm-12">
									<?php /* if(!empty($_GET['notif'])){?>
										<?php if($_GET['notif']=="tambahberhasil"){?>
											<div class="alert alert-success" role="alert">
												Data Berhasil Ditambahkan
											</div>
										<?php } 
										else if($_GET['notif']=="editberhasil"){?>
											<div class="alert alert-success" role="alert">
												Data Berhasil Diubah
											</div>
										<?php }?>
									<?php } */?>
								</div> -->
								<table class="table table-bordered">
									<thead>                  
										<tr>
											<th width="5%">NO</th>
											<th width="10%">NIK</th>
											<th width="10%">Nama</th>
											<th width="10%">Pend. Akhir</th>
											<th width="10%">Pengalaman</th>
											<th width="13%">
												<center>Aksi</center>
											</th>
										</tr>
									</thead>
									<tbody>
										<?php
											
											while($data_h = mysqli_fetch_row($query_h)){
												$nik = $data_h[0];
												$nama = $data_h[1];
												$pend_akhir = $data_h[3];
												$pengalaman = $data_h[5];
										?>	
										<tr>
											<td><?= "$no"; ?></td>
											<td><?= "$nik"; ?></td>
											<td><?= "$nama"; ?></td>
											<td><?= "$pend_akhir"; ?></td>
											<td><?= "$pengalaman"; ?></td>
											<td align="center">
												<!-- <a href="index.php?include=edit_cabang&data=<?php echo $kode_cabang;?>" class="btn btn-xs btn-info">
													<i class="fas fa-edit"></i> Edit
												</a> -->
												<a href="javascript:if(confirm('Anda yakin ingin menghapus data <?php echo $nama; ?>?'))window.location.href = 'index.php?include=karyawan_baru&aksi=hapus&data=<?php echo $nik;?>'" class="btn btn-xs btn-warning">
													<i class="fas fa-trash"></i> Hapus 
												</a> 
												<a href="index.php?include=detail_karyawan_baru&data=<?php echo $nik;?>" class="btn btn-xs btn-info" title="Detail">
													<i class="fas fa-eye"></i>
													Detail
												</a>
											</td>
										</tr>
										<?php
											$no++;
										}?>
										<?php 
											//hitung jumlah semua data 
											$sql_jum = "select * from `karyawan_baru`"; 
											if (isset($_SESSION['kataKunci_karyawanBaru'])){
												$kataKunci_karyawanBaru = $_SESSION['kataKunci_karyawanBaru'];
												$sql_jum .= " where `nik` LIKE '%$kataKunci_karyawanBaru%'";
												$sql_jum .= " or `nama` LIKE '%$kataKunci_karyawanBaru%'";
											} 
											$sql_jum .= " order by `nik`";
											$query_jum = mysqli_query($koneksi,$sql_jum);
											$jum_data = mysqli_num_rows($query_jum);
											$jum_halaman = ceil($jum_data/$batas);
										?>
									</tbody>
								</table>
							</div>
							<!-- /.card-body -->
							<div class="card-footer clearfix">
								<span>Showing <?php if($jum_data>0){echo $x;}else {echo 0;}?> to <?php echo $no-1;?> of <?php echo $jum_data;?> entries</span>
								<ul class="pagination pagination-sm m-0 float-right">
									<?php 
										if($jum_halaman==0){
											//tidak ada halaman
										}else if($jum_halaman==1){
											echo "<li class='page-item'><a class='page-link'>1</a></li>";
										}else{
											$sebelum = $halaman-1;
											$setelah = $halaman+1;                  
											if($halaman!=1){
												echo "<li class='page-item'><a class='page-link' href='index.php?include=karyawan_baru&halaman=1'>First</a></li>";
												echo "<li class='page-item'><a class='page-link' href='index.php?include=karyawan_baru&halaman=$sebelum'>«</a></li>";
											}
											//menampilkan angka halaman
											for($i=1; $i<=$jum_halaman; $i++){
												if($i!=$halaman){
													echo "<li class='page-item'><a class='page-link' href='index.php?include=karyawan_baru&halaman=$i'>$i</a></li>";
												}else{
													echo "<li class='page-item'><a class='page-link'>$i</a></li>";
												}
											}
											if($halaman!=$jum_halaman){
												echo "<li class='page-item'><a class='page-link'  href='index.php?include=karyawan_baru&halaman=$setelah'>»</a></li>";
												echo "<li class='page-item'><a class='page-link' href='index.php?include=karyawan_baru&halaman=$jum_halaman'>Last</a></li>";
											}
										}
									?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Right sidebar -->
	<!-- ============================================================== -->
	<!-- .right-sidebar -->
	<!-- ============================================================== -->
	<!-- End Right sidebar -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->