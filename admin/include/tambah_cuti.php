<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">
                <i class="mr-3 fas fa-plus" aria-hidden="true"></i>
                Tambah Cuti
            </h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.php?include=dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item" aria-current="page"><a href="cuti.php">Cuti</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah Cuti</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-right upgrade-btn">
                <a href="index.php?include=cuti" class="btn btn-sm btn-warning d-none d-md-inline-block text-white">
                    <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- column -->
        <div class="col-sm-12">
            <div class="card">
                <div class="row">
                    <!-- column -->
                    <div class="col-sm-12">
                        <div class="card-body">
                            <section class="content">
                                <div class="card card-info">
                                    <div class="card-header">
                                        <h3 class="card-title" style="margin-top:5px;">
                                            <i class="far fa-list-alt"></i> Form Tambah Cuti
                                        </h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <br>
                                    <div class="col-sm-10">
                                        <?php if((!empty($_GET['notif']))&&(!empty($_GET['jenis']))){?>
											<?php if($_GET['notif']=="tambahkosong"){?>
												<div class="alert alert-danger" role="alert">Maaf data
													<?php echo $_GET['jenis'];?> wajib di isi
												</div>
											<?php }?>
                                        <?php }?>
                                    </div>
                                    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="index.php?include=konfirmasi_tambah_cuti">
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label for="nik" class="col-sm-3 col-form-label">NIK</label>
                                                <div class="col-sm-7">
                                                    <select class="form-control" id="nik" name="nik">
                                                        <option value=""></option>
                                                        <?php
															$sql_cbox_nik = "SELECT `NIK` FROM `karyawan` ORDER BY `NIK`";
															$query_cbox_nik = mysqli_query($koneksi,$sql_cbox_nik);
															while($data_cbox_nik = mysqli_fetch_row($query_cbox_nik)){
																	$cbox_nik = $data_cbox_nik[0];
														?>
                                                        <option value="<?php echo $cbox_nik;?>" <?php if(!empty($_SESSION['nik'])){if($cbox_nik==$_SESSION['nik']){ ?> selected="selected" <?php }}?>>
                                                            <?php echo $cbox_nik;?>
														</option>
														<?php
															} 
														?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="bulan" class="col-sm-3 col-form-label">Bulan</label>
                                                <div class="col-sm-7">
                                                    <!-- <input type="text" name="bulan"  class="form-control" id="bulan" value="<?php //if(!empty($_SESSION['bulan'])){ echo $_SESSION['bulan'];} ?>"> -->
                                                    <select class="form-control" id="bulan" name="bulan">
                                                        <option value=""></option>
                                                        <?php
															$bulan = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
															$cbox=0;
															while($cbox < count($bulan)){
														?>
                                                        <option value="<?php echo $bulan[$cbox];?>" <?php if(!empty($_SESSION['bulan'])){if($bulan[$cbox]==$_SESSION['bulan']){ ?> selected="selected" <?php }}?>>
                                                            <?php echo $bulan[$cbox];?>
														</option>
														<?php
															$cbox++;
															} 
														?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="tahun" class="col-sm-3 col-form-label">Tahun</label>
                                                <div class="col-sm-7">
                                                    <input type="number" min="1" name="tahun" class="form-control" id="tahun" value="<?php if(!empty($_SESSION['tahun'])){ echo $_SESSION['tahun'];} ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="jumlah cuti" class="col-sm-3 col-form-label">Jumlah Cuti</label>
                                                <div class="col-sm-7">
                                                    <input type="number" min="1" name="jumlah_cuti" class="form-control" id="jumlah_cuti" value="<?php if(!empty($_SESSION['jumlah_cuti'])){ echo $_SESSION['jumlah_cuti'];} ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-info float-right">
                                                <i class="mr-3 fas fa-plus" aria-hidden="true"></i>Tambah
                                            </button>
                                        </div>
                                        <!-- /.card-footer -->
                                    </form>
                                </div>
                                <!-- /.card -->
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->