<?php
	if(isset($_GET['data'])){
		$nik = $_GET['data'];
		$_SESSION['NIK']=$nik;
		//get data mahasiswa
		$sql_m ="SELECT `k`.`NIK`,`k`.`Nama`,`k`.`tempat_lahir`, `k`.`tanggal_lahir`,`k`.`jenis_kel`,`k`.`agama`,
				`k`.`alamat`,`j`.`kode_jabatan`,`c`.`kode_cabang`,`d`.`kode_departemen` from `karyawan` `k` inner join 
				`jabatan` `j` on `k`.`kode_jabatan` = `j`.`kode_jabatan` INNER JOIN  `departemen` `d` ON `k`.`kode_departemen`=`d`.`kode_departemen` 
				INNER JOIN `cabang` `c` ON `k`.`kode_cabang`= `c`.`kode_cabang` where `k`.`NIK`= '$nik'";
		$query_m = mysqli_query($koneksi,$sql_m);
		while($data_m = mysqli_fetch_row($query_m)){
			$nik = $data_m[0];
			$nama = $data_m[1];
			$tempat_lahir = $data_m[2];
			$tanggal_lahir = $data_m[3];
			$jenis_kelamin=$data_m[4];
			$agama = $data_m[5];
			$alamat = $data_m[6];
			$kode_jabatan=$data_m[7];
			$kode_cabang=$data_m[8];
			$kode_departemen = $data_m[9];
		}
	}
?>
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">
                <i class="mr-3 fas fa-pencil-alt" aria-hidden="true"></i>
                Edit Data Karyawan
            </h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.php?include=dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Karyawan</li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Data Karyawan</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-right upgrade-btn">
                <a href="index.php?include=karyawan" class="btn btn-sm btn-warning d-none d-md-inline-block text-white">
                    <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- column -->
        <div class="col-sm-12">
            <div class="card">
                <div class="row">
                    <!-- column -->
                    <div class="col-sm-12">
                        <div class="card-body">
                            <section class="content">
                                <div class="card card-info">
                                    <div class="card-header">
                                        <h3 class="card-title" style="margin-top:5px;">
                                            <i class="far fa-list-alt"></i> Form Edit Data Karyawan
                                        </h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <br>
                                    <div class="col-sm-10">
                                        <?php if((!empty($_GET['notif']))&&(!empty($_GET['jenis']))){?>
                                        <?php if($_GET['notif']=="editkosong"){?>
                                        <div class="alert alert-danger" role="alert">
                                            Maaf data
                                            <?php echo $_GET['jenis'];?> wajib di isi
                                        </div>
                                        <?php }?>
                                        <?php }?>
                                    </div>
                                    <form class="form-horizontal" method="post" action="index.php?include=konfirmasi_edit_karyawan">
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label for="nama" class="col-sm-3 col-form-label">NIK</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="nik" name="nik" value="<?php echo $nik;?>" readonly="readonly">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="nama" class="col-sm-3 col-form-label">Nama</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="nama" name="nama" value="<?php echo $nama;?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="nama" class="col-sm-3 col-form-label">Tempat Lahir</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="tempat" name="tempat" value="<?php echo $tempat_lahir;?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="nama" class="col-sm-3 col-form-label">Tanggal Lahir</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="tanggal" name="tanggal" value="<?php echo $tanggal_lahir;?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="email" class="col-sm-3 col-form-label">Jenis Kelamin</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="jenis_kelamin" name="jenis_kelamin" value="<?php echo $jenis_kelamin;?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="nama" class="col-sm-3 col-form-label">Agama</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="agama" name="agama" value="<?php echo $agama;?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="nama" class="col-sm-3 col-form-label">Alamat</label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="alamat" name="alamat" value="<?php echo $alamat;?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="jurusan" class="col-sm-3 col-form-label">Jabatan</label>
                                                <div class="col-sm-7">
                                                    <select class="form-control" id="jabatan" name="jabatan">
                                                        <option value="0">-Pilih Jabatan-</option>
                                                        <?php
																		   $sql_j ="select `kode_jabatan`,`nama_jabatan` from `jabatan` order by `kode_jabatan`";
																		   $query_j = mysqli_query($koneksi,$sql_j);
																				while($data_j = mysqli_fetch_row($query_j)){
																					$kode_jabatan = $data_j[0];
																					$nama_jabatan = $data_j[1];
																		   ?>
                                                        <option value="<?php echo $kode_jabatan;?>" <?php if($kode_jabatan==$kode_jabatan){?> selected="selected" <?php }?>>
                                                            <?php echo "$kode_jabatan - $nama_jabatan";?>
                                                            <?php }?>
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="jurusan" class="col-sm-3 col-form-label">Cabang</label>
                                                <div class="col-sm-7">
                                                    <select class="form-control" id="cabang" name="cabang">
                                                        <option value="0">- Pilih cabang-</option>
                                                        <?php
																				$sql_j ="select `kode_cabang`,`nama_cabang` from `cabang` order by `kode_cabang`";
																				$query_j = mysqli_query($koneksi,$sql_j);
																					while($data_j = mysqli_fetch_row($query_j)){
																						$kode_cabang = $data_j[0];
																						$nama_cabang = $data_j[1];
																			?>
                                                        <option value="<?php echo $kode_cabang;?>" <?php if($kode_cabang==$kode_cabang){?> selected="selected" <?php }?>>
                                                            <?php echo "$kode_cabang - $nama_cabang";?>
                                                            <?php }?>
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="jurusan" class="col-sm-3 col-form-label">Departemen</label>
                                                <div class="col-sm-7">
                                                    <select class="form-control" id="departemen" name="departemen">
                                                        <option value="0">-Pilih Departemen-</option>
                                                        <?php
																		   $sql_j ="select `kode_departemen`,`nama_departemen` from `departemen` order by `kode_departemen`";
																		   $query_j = mysqli_query($koneksi,$sql_j);
																				while($data_j = mysqli_fetch_row($query_j)){
																					$kode_departemen = $data_j[0];
																					$nama_departemen = $data_j[1];
																		   ?>
                                                        <option value="<?php echo $kode_departemen;?>" <?php if($kode_departemen==$kode_departemen){?> selected="selected" <?php }?>>
                                                            <?php echo "$kode_departemen - $nama_departemen";?>
                                                            <?php }?>
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-info float-right">
                                                <i class="mr-3  far fa-save" aria-hidden="true"></i>Simpan
                                            </button>
                                        </div>
                                        <!-- /.card-footer -->
                                    </form>
                                </div>
                                <!-- /.card -->
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->