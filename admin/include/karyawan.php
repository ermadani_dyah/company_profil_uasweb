<?php 
	if((isset($_GET['aksi']))&&(isset($_GET['data']))){
		if($_GET['aksi']=='hapus'){
			$nik = $_GET['data'];
			$sql_dm = "delete from `karyawan` where `NIK` = '$nik'";
			mysqli_query($koneksi,$sql_dm);
		}
	}
?>
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">
                Karyawan
            </h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
							<a href="index.php?include=dashboard">Home</a>
						</li>
                        <li class="breadcrumb-item active" aria-current="page">Karyawan</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-right upgrade-btn">
                <a href="index.php?include=tambah_karyawan" class="btn btn-success d-none d-md-inline-block text-white">
                    <i class="mr-3 far fa-plus-square" aria-hidden="true"></i>
                    Tambah Data
                </a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- column -->
        <div class="col-sm-12">
            <div class="card">
                <div class="row">
                    <!-- column -->
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-header">
                                    <h3 class="card-title" style="margin-top:5px;">
                                        <i class="mr-3 fas fa-user-circle" aria-hidden="true"></i> Daftar Karyawan
                                    </h3>
                                </div>
                                <div class="card-body">
                                    <div class="col-md-12">
                                        <form method="post" action="index.php?include=karyawan">
                                            <div class="row">
                                                <div class="col-md-4 bottom-10">
                                                    <input type="text" class="form-control" id="kata_kunci" name="katakunci" placeholder="Cari NIK / Nama">
                                                </div>
                                                <div class="col-md-5 bottom-10">
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="mr-3 fas fa-search" aria-hidden="true"></i>&nbsp; Search
													</button>
                                                </div>
                                            </div><!-- .row -->
                                        </form>
                                    </div><br>
                                    <div class="col-sm-12">
                                        <?php if(!empty($_GET['notif'])){?>
											<?php if($_GET['notif']=="tambahberhasil"){?>
												<div class="alert alert-success" role="alert">
													Data Berhasil Ditambahkan
												</div>
											<?php } else if($_GET['notif']=="editberhasil"){?>
												<div class="alert alert-success" role="alert">
													Data Berhasil Diubah
												</div>
											<?php }?>
                                        <?php }?>
                                    </div>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th width="5%">NO</th>
                                                <th width="15%">NIK</th>
                                                <th width="15%">Nama</th>
                                                <th width="5%">Jabatan</th>
                                                <th width="5%">Cabang</th>
                                                <th width="5%">Departemen</th>
                                                <th width="20%">
                                                    <center>Aksi</center>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
												//menampilkan data hobi
												$batas = 3;
												if(!isset($_GET['halaman'])){
													$posisi = 0;
													$halaman = 1;	
												}else{
													$halaman = $_GET['halaman'];
													$posisi = ($halaman-1) * $batas;
												} 
												$x = $posisi+1;
												$sql_h = "SELECT `k`.`NIK`,`k`.`Nama`,`k`.`tempat_lahir`, `k`.`tanggal_lahir`,`k`.`jenis_kel`,`k`.`agama`,
														`k`.`alamat`,`j`.`kode_jabatan`,`c`.`kode_cabang`,`d`.`kode_departemen` from `karyawan` `k` inner join 
														`jabatan` `j` on `k`.`kode_jabatan` = `j`.`kode_jabatan` INNER JOIN  `departemen` `d` ON `k`.`kode_departemen`=`d`.`kode_departemen` 
														INNER JOIN `cabang` `c` ON `k`.`kode_cabang`= `c`.`kode_cabang`"; 
												if (isset($_POST["katakunci"])){
													$katakunci_karyawan = $_POST["katakunci"];
													$_SESSION['katakunci_karyawan'] = $katakunci_karyawan;
													$sql_h .= " where `k`.`NIK` LIKE '%$katakunci_karyawan%' 
																OR `Nama` LIKE '%$katakunci_karyawan%' ";
												} 
												$sql_h .= "order by `NIK` limit $posisi, $batas";
												$query_h = mysqli_query($koneksi,$sql_h);
												$no=$posisi+1;
												while($data_h = mysqli_fetch_row($query_h)){
													$nik = $data_h[0];
													$nama = $data_h[1];
													$tempat_lahir = $data_h[2];
													$tanggal_lahir = $data_h[3];
													$jenis_kelamin=$data_h[4];
													$agama = $data_h[5];
													$alamat = $data_h[6];
													$kode_jabatan=$data_h[7];
													$kode_cabang=$data_h[8];
													$kode_departemen = $data_h[9];
												?>
                                            <tr>
                                                <td><?php echo $no;?></td>
                                                <td><?php echo $nik;?></td>
                                                <td><?php echo $nama;?></td>
                                                <td><?php echo $kode_cabang;?></td>
                                                <td><?php echo $kode_jabatan;?></td>
                                                <td><?php echo $kode_departemen;?></td>
                                                <td>
                                                    <a href="index.php?include=edit_karyawan&data=<?php echo $nik;?>" class="btn btn-xs btn-info">
                                                        <i class="fas fa-edit"></i> Edit
                                                    </a>
                                                    <a href="javascript:if(confirm('Anda yakin ingin menghapus data <?php echo $nama; ?>?'))window.location.href = 'index.php?include=karyawan&aksi=hapus&data=<?php echo $nik;?>'" class="btn btn-xs btn-warning">
                                                        <i class="fas fa-trash"></i> Hapus
                                                    </a>
													<a href="index.php?include=detail_karyawan&data=<?php echo $nik;?>" class="btn btn-xs btn-info" title="Detail">
														<i class="fas fa-eye"></i>
														Detail
													</a>
                                                </td>
                                            </tr>
                                            <?php
												$no++;
											}?>
                                            <?php 
												//hitung jumlah semua data 
												$sql_jum = "SELECT `k`.`NIK`,`k`.`Nama`,`k`.`tempat_lahir`, `k`.`tanggal_lahir`,`k`.`jenis_kel`,`k`.`agama`,
															`k`.`alamat`,`j`.`kode_jabatan`,`c`.`kode_cabang`,`d`.`kode_departemen` from `karyawan` `k` inner join 
															`jabatan` `j` on `k`.`kode_jabatan` = `j`.`kode_jabatan` INNER JOIN  `departemen` `d` ON `k`.`kode_departemen`=`d`.`kode_departemen` 
															INNER JOIN `cabang` `c` ON `k`.`kode_cabang`= `c`.`kode_cabang`"; 
												if (isset($_SESSION['katakunci_karyawan'])){
													$katakunci_karyawan = $_SESSION['katakunci_karyawan'];
													$sql_jum .= " where `k`.`NIK` LIKE '%$katakunci_karyawan%'
																OR `Nama` LIKE '%$katakunci_karyawan%' ";
												} 
												$sql_jum .= " order by `k`.`NIK`";
												$query_jum = mysqli_query($koneksi,$sql_jum);
												$jum_data = mysqli_num_rows($query_jum);
												$jum_halaman = ceil($jum_data/$batas);
											?>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer clearfix">
                                    <span>Showing <?php if($jum_data>0){echo $x;}else {echo 0;}?> to <?php echo $no-1;?> of <?php echo $jum_data;?> entries</span>
                                    <ul class="pagination pagination-sm m-0 float-right">
                                        <?php 
											if($jum_halaman==0){
												//tidak ada halaman
											}else if($jum_halaman==1){
												echo "<li class='page-item'><a class='page-link'>1</a></li>";
											}else{
												$sebelum = $halaman-1;
												$setelah = $halaman+1;                  
												if($halaman!=1){
													echo "<li class='page-item'><a class='page-link' href='index.php?include=karyawan&halaman=1'>First</a></li>";
													echo "<li class='page-item'><a class='page-link' href='index.php?include=karyawan&halaman=$sebelum'>«</a></li>";
												}
													//menampilkan angka halaman
												for($i=1; $i<=$jum_halaman; $i++){
													if($i!=$halaman){
														echo "<li class='page-item'><a class='page-link' href='index.php?include=karyawan&halaman=$i'>$i</a></li>";
													}else{
														echo "<li class='page-item'><a class='page-link'>$i</a></li>";
													}
												}
												if($halaman!=$jum_halaman){
													echo "<li class='page-item'><a class='page-link'  href='index.php?include=karyawan&halaman=$setelah'>»</a></li>";
													echo "<li class='page-item'><a class='page-link' href='index.php?include=karyawan&halaman=$jum_halaman'>Last</a></li>";
												}
											}
										?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->