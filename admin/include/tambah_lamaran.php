<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">
                <i class="mr-3 fas fa-plus" aria-hidden="true"></i>
                Tambah Berita Lamaran
            </h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.php?include=dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Lamaran</li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah Berita Lamaran</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-right upgrade-btn">
                <a href="index.php?include=lamaran" class="btn btn-sm btn-warning d-none d-md-inline-block text-white">
                    <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- column -->
        <div class="col-sm-12">
            <div class="card">
                <div class="row">
                    <!-- column -->
                    <div class="col-sm-12">
                        <div class="card-body">
                            <section class="content">
                                <div class="card card-info">
                                    <div class="card-header">
                                        <h3 class="card-title" style="margin-top:5px;">
                                            <i class="far fa-list-alt"></i> Form Tambah Berita Lamaran
                                        </h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <br>
                                    <div class="col-sm-10">
                                        <?php if((!empty($_GET['notif']))&&(!empty($_GET['jenis']))){?>
											<?php if($_GET['notif']=="tambahkosong"){?>
												<div class="alert alert-danger" role="alert">Maaf data
													<?php echo $_GET['jenis'];?> wajib di isi
												</div>
											<?php }?>
                                        <?php }?>
                                    </div>
                                    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="index.php?include=konfirmasi_tambah_lamaran">
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label for="hobi" class="col-sm-3 col-form-label">Kode Lamaran</label>
                                                <div class="col-sm-7">
                                                    <input type="text" name="lamaran" class="form-control" id="lamaran" value="<?php if(!empty($_SESSION['lamaran'])){ echo $_SESSION['lamaran'];} ?>"></div>
                                                </div>
                                            <div class="form-group row">
                                                <label for="hobi" class="col-sm-3 col-form-label">Syarat</label>
                                                <div class="col-sm-7">
                                                    <textarea class="form-control" id="syarat" name="syarat" cols="30" rows="7" value="<?php if(!empty($_SESSION['syarat'])){ echo $_SESSION['syarat'];} ?>"><?php if(!empty($_SESSION['syarat'])){ echo $_SESSION['syarat'];} ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="hobi" class="col-sm-3 col-form-label">Cabang Perusahaan</label>
                                                <div class="col-sm-7">
                                                    <select class="form-control" id="cabang" name="cabang">
                                                        <option value="0">-Pilih Cabang-</option>
                                                        <?php
															$sql_j ="select `kode_cabang`,`nama_cabang` from `cabang` order by `kode_cabang`";
															$query_j = mysqli_query($koneksi,$sql_j);
															while($data_j = mysqli_fetch_row($query_j)){
																$kode_cabang = $data_j[0];
																$nama_cabang = $data_j[1];
															?>
                                                        <option value="<?php echo $kode_cabang;?>" <?php if(!empty($_SESSION['kode_cabang'])){if($kode_cabang==$_SESSION['kode_cabang']){?> selected="selected" <?php }}?>>
                                                            <?php echo "$kode_cabang - $nama_cabang";?>
                                                            <?php }?>
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="hobi" class="col-sm-3 col-form-label">Bagian</label>
                                                <div class="col-sm-7">
                                                    <input type="text" name="job" class="form-control" id="job" value="<?php if(!empty($_SESSION['job'])){ echo $_SESSION['job'];} ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-info float-right">
                                                <i class="mr-3 fas fa-plus" aria-hidden="true"></i>Tambah
                                            </button>
                                        </div>
                                        <!-- /.card-footer -->
                                    </form>
                                </div>
                                <!-- /.card -->
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->