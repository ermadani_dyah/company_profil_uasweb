<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">
                <i class="mr-3 fas fa-plus" aria-hidden="true"></i>
                Tambah Departemen
            </h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
							<a href="index.php?include=dashboard">Home</a>
						</li>
                        <li class="breadcrumb-item active" aria-current="page">Departemen</li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah Departemen</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-right upgrade-btn">
                <a href="index.php?include=departemen" class="btn btn-sm btn-warning d-none d-md-inline-block text-white">
                    <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- column -->
        <div class="col-sm-12">
            <div class="card">
                <div class="row">
                    <!-- column -->
                    <div class="col-sm-12">
                        <div class="card-body">
                            <section class="content">
                                <div class="card card-info">
                                    <div class="card-header">
                                        <h3 class="card-title" style="margin-top:5px;">
                                            <i class="far fa-list-alt"></i> Form Tambah Departemen
                                        </h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <br>
                                    <div class="col-sm-10">
                                        <?php if((!empty($_GET['notif']))&&(!empty($_GET['jenis']))){?>
											<?php if($_GET['notif']=="tambahkosong"){?>
												<div class="alert alert-danger" role="alert">Maaf data
													<?php echo $_GET['jenis'];?> wajib di isi
												</div>
											<?php }?>
                                        <?php }?>
                                    </div>
                                    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="index.php?include=konfirmasi_tambah_departemen">
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label for="hobi" class="col-sm-3 col-form-label">Kode Departemen</label>
                                                <div class="col-sm-7">
                                                    <input type="text" name="kode_departemen" class="form-control" id="kode_departemen" value="<?php if(!empty($_SESSION['kode_departemen'])){ echo $_SESSION['kode_departemen'];} ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="hobi" class="col-sm-3 col-form-label">Nama Departemen</label>
                                                <div class="col-sm-7">
                                                    <input type="text" name="nama_departemen" class="form-control" id="nama_departemen" value="<?php if(!empty($_SESSION['nama_departemen'])){ echo $_SESSION['nama_departemen'];} ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="hobi" class="col-sm-3 col-form-label">Deskripsi</label>
                                                <div class="col-sm-7">
                                                    <input type="text" name="deskripsi" class="form-control" id="deskripsi" value="<?php if(!empty($_SESSION['deskripsi'])){ echo $_SESSION['deskripsi'];} ?>">   
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-info float-right">
                                                <i class="mr-3 fas fa-plus" aria-hidden="true"></i>Tambah
                                            </button>
                                        </div>
                                        <!-- /.card-footer -->
                                    </form>
                                </div>
                                <!-- /.card -->
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->