<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">
                <i class="mr-3 fas fa-plus" aria-hidden="true"></i>
                Tambah User
            </h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
							<a href="index.php?include=dashboard">Home</a>
						</li>
                        <li class="breadcrumb-item active" aria-current="page">User</li>
                        <li class="breadcrumb-item active" aria-current="page">Tambah User</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-right upgrade-btn">
                <a href="index.php?include=user" class="btn btn-sm btn-warning d-none d-md-inline-block text-white">
                    <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- column -->
        <div class="col-sm-12">
            <div class="card">
                <div class="row">
                    <!-- column -->
                    <div class="col-sm-12">
                        <div class="card-body">
                            <section class="content">
                                <div class="card card-info">
                                    <div class="card-header">
                                        <h3 class="card-title" style="margin-top:5px;">
                                            <i class="far fa-list-alt"></i> Form Tambah Data User
                                        </h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <br>
                                    <div class="col-sm-10">
                                        <?php if((!empty($_GET['notif']))&&(!empty($_GET['jenis']))){?>
                                        <?php if($_GET['notif']=="tambahkosong"){?>
                                        <div class="alert alert-danger" role="alert">Maaf data
                                            <?php echo $_GET['jenis'];?> wajib di isi
                                        </div>
                                        <?php }?>
                                        <?php }?>
                                    </div>
                                    <form class="form-horizontal" enctype="multipart/form-data" method="post" action="index.php?include=konfirmasi_tambah_user">
                                        <div class="card-body">
                                            <div class="form-group row">
                                                <label for="hobi" class="col-sm-3 col-form-label">Nama Lengkap</label>
                                                <div class="col-sm-7">
                                                    <input type="text" name="nama" class="form-control" id="nama" value="<?php if(!empty($_SESSION['nama_lengkap'])){ echo $_SESSION['nama_lengkap'];} ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="hobi" class="col-sm-3 col-form-label">Nama Panggilan</label>
                                                <div class="col-sm-7">
                                                    <input type="text" name="panggilan" class="form-control" id="panggilan" value="<?php if(!empty($_SESSION['nama'])){ echo $_SESSION['nama'];} ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="hobi" class="col-sm-3 col-form-label">Email</label>
                                                <div class="col-sm-7">
                                                    <input type="text" name="email" class="form-control" id="email" value="<?php if(!empty($_SESSION['email'])){ echo $_SESSION['email'];} ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="hobi" class="col-sm-3 col-form-label">No HP</label>
                                                <div class="col-sm-7">
                                                    <input type="text" name="hp" class="form-control" id="hp" value="<?php if(!empty($_SESSION['no_hp'])){ echo $_SESSION['no_hp'];} ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="hobi" class="col-sm-3 col-form-label">Username</label>
                                                <div class="col-sm-7">
                                                    <input type="text" name="username" class="form-control" id="username" value="<?php if(!empty($_SESSION['username'])){ echo $_SESSION['username'];} ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="password" class="col-sm-3 col-form-label">Password</label>
                                                <div class="col-sm-7">
                                                    <input type="password" class="form-control" name="password" id="password" value="<?php if(!empty($_SESSION['password'])){ echo $_SESSION['password'];} ?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="level" class="col-sm-3 col-form-label">Level</label>
                                                <div class="col-sm-7">
                                                    <select class="form-control" id="level" name="level">
                                                        <option value="0">- Pilih level -</option>
                                                        <option value="kepala">Kepala Keuangan</option>
                                                        <option value="staff">Staff</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.card-body -->
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-info float-right">
                                                <i class="mr-3 fas fa-plus" aria-hidden="true"></i>Tambah
                                            </button>
                                        </div>
                                        <!-- /.card-footer -->
                                    </form>
                                </div>
                                <!-- /.card -->
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->