<?php 
	if(isset($_GET['data'])){
		$nik = $_GET['data'];
		//get data karyawan
		$sql_m = "SELECT `k`.`NIK`,`k`.`Nama`,`k`.`tempat_lahir`, `k`.`tanggal_lahir`,`k`.`jenis_kel`,`k`.`agama`,
				`k`.`alamat`,`j`.`nama_jabatan`,`c`.`nama_cabang`,`d`.`nama_departemen` from `karyawan` `k` inner join 
				`jabatan` `j` on `k`.`kode_jabatan` = `j`.`kode_jabatan` INNER JOIN  `departemen` `d` ON `k`.`kode_departemen`=`d`.`kode_departemen` 
				INNER JOIN `cabang` `c` ON `k`.`kode_cabang`= `c`.`kode_cabang` where `k`.`NIK`='$nik'";
		$query_m = mysqli_query($koneksi,$sql_m);
		while($data_m = mysqli_fetch_row($query_m)){
			$nik = $data_m[0];
			$nama = $data_m[1];
			$tempat_lahir = $data_m[2];
			$tanggal_lahir = $data_m[3];
			$jenis_kelamin=$data_m[4];
			$agama = $data_m[5];
			$alamat = $data_m[6];
			$kode_jabatan=$data_m[7];
			$kode_cabang=$data_m[8];
			$kode_departemen = $data_m[9];
		}	
	}
?>
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">
                <i class="mr-3 fas fa-pencil-alt" aria-hidden="true"></i>
                Detail Data Karyawan
            </h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.php?include=dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">Data Karyawan </li>
                        <li class="breadcrumb-item active" aria-current="page">Detail Data Karyawan</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-right upgrade-btn">
                <a href="index.php?include=karyawan" class="btn btn-sm btn-warning d-none d-md-inline-block text-white">
                    <i class="mr-3  fas fa-arrow-left" aria-hidden="true"></i>
                    Kembali
                </a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- column -->
        <div class="col-sm-12">
            <div class="card">
                <div class="row">
                    <!-- column -->
                    <div class="col-sm-12">
                        <div class="card-body">
                            <section class="content">
                                <div class="card card-info">
                                    <div class="card-header">
                                        <h3 class="card-title" style="margin-top:5px;">
                                            <i class="far fa-list-alt"></i> Form Data Karyawan
                                        </h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <!-- form start -->
                                    <br>
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td colspan="2">
                                                    <i class="fas fa-user-circle"></i>
                                                    <strong>Profil Karyawan</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    <strong>NIK</strong>
                                                </td>
                                                <td width="80%">
                                                    <?php echo $nik;?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    <strong>Nama</strong>
                                                </td>
                                                <td width="80%">
                                                    <?php echo $nama;?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20%">
                                                    <strong>Tempat Lahir</strong>
                                                </td>
                                                <td width="80%">
                                                    <?php echo $tempat_lahir;?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>Tanggal Lahir<strong>
                                                </td>
                                                <td width="80%">
                                                    <?php echo $tanggal_lahir;?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>Jenis Kelamin<strong>
                                                </td>
                                                <td width="80%">
                                                    <?php echo $jenis_kelamin;?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>Agama<strong>
                                                </td>
                                                <td width="80%">
                                                    <?php echo $agama;?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>Alamat<strong>
                                                </td>
                                                <td width="80%">
                                                    <?php echo $alamat;?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>Jabatan<strong>
                                                </td>
                                                <td width="80%">
                                                    <?php echo $kode_jabatan;?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>Departemen<strong>
                                                </td>
                                                <td width="80%">
                                                    <?php echo $kode_departemen;?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <strong>Cabang<strong>
                                                </td>
                                                <td width="80%">
                                                    <?php echo $kode_cabang;?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.card -->
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->