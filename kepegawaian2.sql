-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 12 Des 2020 pada 06.49
-- Versi server: 10.4.16-MariaDB
-- Versi PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kepegawaian2`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `kolom1` varchar(255) NOT NULL,
  `jasa` varchar(255) NOT NULL,
  `pendiri` varchar(225) NOT NULL,
  `kolom2` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `about`
--

INSERT INTO `about` (`id`, `kolom1`, `jasa`, `pendiri`, `kolom2`) VALUES
(1, 'PT Lancaar jaya berdiri sejak tahun 2010 berawal dari nama ace administrasi lalu pada tahun 2015 berganti nama menjadi PT Lancar Jaya', 'kami PT Lancar Jaya berpengalaman dalam menyediakan produk dan layanan terbaik dalam dunia IT', 'PT Lancar jaya di dirikan oleh 10 lulusan terbaik ITS,ITB,UI dan berbagi kampus terkenal lain', 'PT Lancar Jaya berkomitmen dalam memberikan solusi IT untuk membantu bisnis anda. Kami percaya bahwa konsisten dan komitmen yang kami miliki telah membuat kami menjadi salah satu perusahaan IT Terbesar Di Indonesia');

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `no_hp` varchar(50) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `nama`, `nama_lengkap`, `email`, `no_hp`, `username`, `password`, `level`) VALUES
(1, 'Dyah', 'Ermadani Dyah', 'ermadani.dyah09@gmail.com', '081555442712', 'ermadani_dyah09', '8fbfd88d05ee870fbf9b12e8b42db844', 'kepala'),
(2, 'jisung', 'park jisung kiyowo', 'park.jisung09@gmail.com', '081555442712', 'park_jisung', '8fbfd88d05ee870fbf9b12e8b42db844', 'staff'),
(13, 'Nana', 'Na Jaemin', 'na.jaemin08@gmail.com', '081555442712', 'nana_jaemin', '8fbfd88d05ee870fbf9b12e8b42db844', 'kepala keuangan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cabang`
--

CREATE TABLE `cabang` (
  `kode_cabang` varchar(50) NOT NULL,
  `nama_cabang` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `email` varchar(65) NOT NULL,
  `no_hp` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `cabang`
--

INSERT INTO `cabang` (`kode_cabang`, `nama_cabang`, `alamat`, `email`, `no_hp`) VALUES
('C001', 'Perusahaan BB2', 'Jl. Kacang Kapri Muda Kav. 13Utan Kayu Selatan, Matraman, Jakarta Timur, Indonesia, 13120', 'bb_production09@gmail.com', '081555442712'),
('C002', 'Perusahaan X', 'Jl. Candi pungur No.09, RT 011/RW 005 Kel. Bandar, Kec. Lowokwaru Kota Malang', 'xx_production08\r\n@gmail.com', '081444552712'),
('C003', 'Perusahaan D', 'Jl. Cinta Boulevard No.3 RT 07/02\r\nBintaro, Pesanggrahan, Jaksel, 12330', 'dd_propro09\r\n@gmail.com', '082333441723'),
('C004', 'Perusahaan F', 'Jl. Kehormatan Blok A No.19 Rt.002 Rw.08\r\nDuri Kepa, Kebon Jeruk, Jakarta Barat, Indonesia, 11510', 'ff_production10\r\n@gmail.com', '2456789976'),
('C005', 'Perusahaan BB2', 'dusun pucang anom desa pucang simo', 'bb_production@gmail.com', '0976253');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cuti`
--

CREATE TABLE `cuti` (
  `nik` varchar(65) NOT NULL,
  `bulan` varchar(65) NOT NULL,
  `tahun` varchar(65) NOT NULL,
  `jml_cut` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `cuti`
--

INSERT INTO `cuti` (`nik`, `bulan`, `tahun`, `jml_cut`) VALUES
('123', 'juni', '2020', '3'),
('125', 'juni', '2020', '3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `departemen`
--

CREATE TABLE `departemen` (
  `kode_departemen` varchar(50) NOT NULL,
  `nama_departemen` varchar(255) NOT NULL,
  `deskripsi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `departemen`
--

INSERT INTO `departemen` (`kode_departemen`, `nama_departemen`, `deskripsi`) VALUES
('D001', 'Help Desk', 'Help Desk adalah titik utama dimana client dari IT akan pertama kali menghubungi divisi IT saat mempunyai pertanyaan atau masalah yang berhubungan dengan IT.'),
('D002', 'End User Support', 'End User Support bertanggung jawab untuk perbaikan fisik komputer dan kunjungan ke lapangan kerja. devisi ini adalah lapisan kedua dari manajemen masalah dan solusi.'),
('D003', 'Network Administration Group', 'Network Administrator Group mengatur semua kemampuan jaringan komunikasi data yang dibutuhkan oleh bisnis. Network administrator bertanggung jawab pada semua kabel, hubs/switch, kemananan jaringan, routers, gateways, firewall, dan hal yang berhubungan den'),
('D004', 'System Administrator / Computer Operation Group', 'System Administrator dan Computer Operations Group mengatur, mengawasi, dan mengkonfigurasi seluruh Server dan System Software yang membentuk sebuah infrastruktur dimana terdapat aplikasi dan data perusahaan.'),
('D005', 'Telecommunications', 'departemeen yang mengatur seluruh telepon dan tugas lainnya yang berhubungan. tugas yang diberikan membenarkan saluran telecomunikasi seperti telepon, voice mail, fax, dan video conferencing.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jabatan`
--

CREATE TABLE `jabatan` (
  `kode_jabatan` varchar(50) NOT NULL,
  `nama_jabatan` varchar(255) NOT NULL,
  `tunjangan_jabatan` bigint(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `jabatan`
--

INSERT INTO `jabatan` (`kode_jabatan`, `nama_jabatan`, `tunjangan_jabatan`) VALUES
('J001', 'Kepala Kuangan', 300000),
('J002', 'Staff Keuangan', 500000),
('J004', 'Kepala Biro', 300000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `karyawan`
--

CREATE TABLE `karyawan` (
  `NIK` varchar(65) NOT NULL,
  `Nama` varchar(255) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` varchar(50) NOT NULL,
  `jenis_kel` varchar(30) NOT NULL,
  `agama` varchar(30) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `kode_jabatan` varchar(50) NOT NULL,
  `kode_cabang` varchar(50) NOT NULL,
  `kode_departemen` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `karyawan`
--

INSERT INTO `karyawan` (`NIK`, `Nama`, `tempat_lahir`, `tanggal_lahir`, `jenis_kel`, `agama`, `alamat`, `kode_jabatan`, `kode_cabang`, `kode_departemen`) VALUES
('123', 'dyah', 'jombang', '09 desember 2000', 'perempuan', 'islam', 'jombang', 'J001', 'C001', 'D001'),
('125', 'Ermadani', 'jombang', '09 desember 2000', 'perempuan', 'islam', 'jombang', 'J002', 'C001', 'D001'),
('1456', 'park jisung', 'korea', '09 desember 2000', 'laki laki', 'hindu', 'soul', 'J002', 'C003', 'D001'),
('1890', 'park jisung', 'korea', '09 desember 2000', 'laki laki', 'hindu', 'soul', 'J002', 'C003', 'D001'),
('345', 'Na Jaemin', 'soul', '10 januari 2000', 'Laki Laki', 'hindu', 'soul', 'J001', 'C002', 'D002'),
('9990', 'Ermadani Dyah R', 'malang', '09 Desember 2000', 'perempuan', 'islam', 'jombangt', 'J001', 'C001', 'D001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `karyawan_baru`
--

CREATE TABLE `karyawan_baru` (
  `nik` bigint(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jk` varchar(10) NOT NULL,
  `pend_akhir` varchar(100) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `pengalaman` text NOT NULL,
  `tanggal_lahir` varchar(65) NOT NULL,
  `email` varchar(65) NOT NULL,
  `agama` varchar(65) NOT NULL,
  `no_hp` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `karyawan_baru`
--

INSERT INTO `karyawan_baru` (`nik`, `nama`, `jk`, `pend_akhir`, `tempat_lahir`, `pengalaman`, `tanggal_lahir`, `email`, `agama`, `no_hp`) VALUES
(123, 'Na Jaemin', 'Laki Laki', 'S1 Teknologi Informasi', 'soul', 'Data analis', '09 juni 2000', 'na.jaemin08@gmail.com', 'hindu', '+6281555442712'),
(5678, 'park jisung kiyowo', 'perempuan', 'D3 Sistem Informasi', 'malang', 'Data analis', '09 desember 2000', 'ermadani.dyah09@gmail.com', 'islam', '976253'),
(9990, 'Oh Sehun', 'Laki Laki', 'S2 Teknik Informatika', 'soul', 'project Manager', '10 januari 2000', 'oh_sehun09@gmail.com', 'hindu', '+6281555442712'),
(567890, 'Na Jaemin', 'Laki Laki', 'D3 Sistem Informasi', 'malang', 'Data analis', '09 desember 2000', 'na.jaemin08@gmail.com', 'hindu', '+6281555442712'),
(6346228, 'Oh Sehun', 'Laki Laki', 'S2 Teknik Informatika', 'soul', 'Data analis', '09 desember 2000', 'oh_sehun09@gmail.com', 'hindu', '+6281555442712'),
(193140914111013, 'ucup', 'cowok', 'sma', 'sidoarjo', 'fresh graduate', '10 oktober 2010', 'ucup09@gmail.com', 'islam', '081555442712'),
(193140914111016, 'dyah', 'cewek', 'sma', 'jombang', 'fresh graduate', '20 desember 2010', 'ermadani.dyah09@gmail.com', 'islam', '081555442712'),
(193140914111026, 'fajar', 'cowok', 'sma', 'nganjuk', 'fresh graduate', '24 desember 2012', 'fajar@gmail.com', 'islam', '081555442712');

-- --------------------------------------------------------

--
-- Struktur dari tabel `karyawan_keuangan`
--

CREATE TABLE `karyawan_keuangan` (
  `nik` varchar(65) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jenis_kelamin` varchar(30) NOT NULL,
  `kode_jabatan` varchar(50) NOT NULL,
  `kode_cabang` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `karyawan_keuangan`
--

INSERT INTO `karyawan_keuangan` (`nik`, `nama`, `jenis_kelamin`, `kode_jabatan`, `kode_cabang`) VALUES
('125', 'ermadani dyah rachmawati', 'perempuan', 'J002', 'C001'),
('1456', 'park jisung kiyowo', 'Laki Laki', 'J001', 'C001'),
('123', 'Rita Sugiarti ', 'Perempuan', 'J001', 'C001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_lamaran`
--

CREATE TABLE `tabel_lamaran` (
  `kode_lamaran` varchar(10) NOT NULL,
  `syarat` varchar(100) NOT NULL,
  `kode_cabang` varchar(10) NOT NULL,
  `bagian` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tabel_lamaran`
--

INSERT INTO `tabel_lamaran` (`kode_lamaran`, `syarat`, `kode_cabang`, `bagian`) VALUES
('L001', '1.FC Ijazah\r\n2.Curriculum Vitae (CV)\r\n3.Portofolio\r\n4.Fotokopi KTP dan Kartu Keluarga\r\n5. Sertifikat', 'C002', 'Technical Writer'),
('L002', '1.FC Ijazah 2.Curriculum Vitae (CV) 3.Portofolio 4.Fotokopi KTP dan Kartu Keluarga 5. Sertifikat', 'C003', 'UI/UX Designer'),
('L003', '1.FC Ijazah 2.Curriculum Vitae (CV) 3.Portofolio 4.Fotokopi KTP dan Kartu Keluarga 5. Sertifikat', 'C002', 'System Analyst'),
('L005', '1.FC Ijazah 2.Curriculum Vitae (CV) 3.Portofolio 4.Fotokopi KTP dan Kartu Keluarga 5. Sertifikat', 'C003', 'Project Manager'),
('LOO4', '1.FC Ijazah 2.Curriculum Vitae (CV) 3.Portofolio 4.Fotokopi KTP dan Kartu Keluarga 5. Sertifikat', 'C003', 'Project Manager');

-- --------------------------------------------------------

--
-- Struktur dari tabel `team`
--

CREATE TABLE `team` (
  `id` int(11) NOT NULL,
  `nama` varchar(65) NOT NULL,
  `pekerjaan` varchar(65) NOT NULL,
  `gambar` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `team`
--

INSERT INTO `team` (`id`, `nama`, `pekerjaan`, `gambar`) VALUES
(1, 'Walter White', 'IT Programmer', 'team-1.jpg'),
(2, 'Sarah Jhonson', 'Analyst Programmer', 'team-2.jpg'),
(3, 'William Anderson', 'IT Executive', 'team-3.jpg'),
(4, 'Amanda Jepson', 'IT Administrator', 'team-4.jpg');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indeks untuk tabel `cabang`
--
ALTER TABLE `cabang`
  ADD PRIMARY KEY (`kode_cabang`);

--
-- Indeks untuk tabel `cuti`
--
ALTER TABLE `cuti`
  ADD KEY `nik` (`nik`);

--
-- Indeks untuk tabel `departemen`
--
ALTER TABLE `departemen`
  ADD PRIMARY KEY (`kode_departemen`);

--
-- Indeks untuk tabel `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`kode_jabatan`);

--
-- Indeks untuk tabel `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`NIK`),
  ADD KEY `kode_jabatan` (`kode_jabatan`,`kode_cabang`,`kode_departemen`),
  ADD KEY `kode_cabang` (`kode_cabang`),
  ADD KEY `kode_departemen` (`kode_departemen`);

--
-- Indeks untuk tabel `karyawan_baru`
--
ALTER TABLE `karyawan_baru`
  ADD PRIMARY KEY (`nik`);

--
-- Indeks untuk tabel `karyawan_keuangan`
--
ALTER TABLE `karyawan_keuangan`
  ADD KEY `nik` (`nik`),
  ADD KEY `kode_jabatan` (`kode_jabatan`),
  ADD KEY `kode_cabang` (`kode_cabang`);

--
-- Indeks untuk tabel `tabel_lamaran`
--
ALTER TABLE `tabel_lamaran`
  ADD PRIMARY KEY (`kode_lamaran`),
  ADD KEY `nama_cabang` (`kode_cabang`);

--
-- Indeks untuk tabel `team`
--
ALTER TABLE `team`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `karyawan_baru`
--
ALTER TABLE `karyawan_baru`
  MODIFY `nik` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=193140914111027;

--
-- AUTO_INCREMENT untuk tabel `team`
--
ALTER TABLE `team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `cuti`
--
ALTER TABLE `cuti`
  ADD CONSTRAINT `cuti` FOREIGN KEY (`nik`) REFERENCES `karyawan` (`NIK`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `karyawan`
--
ALTER TABLE `karyawan`
  ADD CONSTRAINT `karyawan_ibfk_1` FOREIGN KEY (`kode_cabang`) REFERENCES `cabang` (`kode_cabang`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `karyawan_ibfk_2` FOREIGN KEY (`kode_departemen`) REFERENCES `departemen` (`kode_departemen`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `karyawan_ibfk_3` FOREIGN KEY (`kode_jabatan`) REFERENCES `jabatan` (`kode_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `karyawan_keuangan`
--
ALTER TABLE `karyawan_keuangan`
  ADD CONSTRAINT `karyawan_keuangan_ibfk_1` FOREIGN KEY (`nik`) REFERENCES `karyawan` (`NIK`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `karyawan_keuangan_ibfk_2` FOREIGN KEY (`kode_cabang`) REFERENCES `cabang` (`kode_cabang`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `karyawan_keuangan_ibfk_3` FOREIGN KEY (`kode_jabatan`) REFERENCES `jabatan` (`kode_jabatan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `tabel_lamaran`
--
ALTER TABLE `tabel_lamaran`
  ADD CONSTRAINT `lamaran` FOREIGN KEY (`kode_cabang`) REFERENCES `cabang` (`kode_cabang`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
