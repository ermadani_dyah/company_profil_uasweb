<main id="main">  
	<!-- ======= news ======= -->
	<section id="pricing" class="pricing">
        <div class="container" data-aos="fade-up">
			<div class="section-title">
				<h2>LAMARAN PEKERJAAN</h2>
				<h3>Perusahaan <span>X</span></h3>
				<p>Daftarkan diri anda untuk menjadi bagian dari kami </p>
			</div>
			<div class="col-sm-6">
				<?php if((!empty($_GET['notif']))&&(!empty($_GET['jenis']))){?>
					<?php if($_GET['notif']=="tambahkosong"){?>
						<div class="alert alert-danger" role="alert">Maaf data 
						  <?php echo $_GET['jenis'];?> wajib di isi
						</div>
					<?php }?>
				<?php }?>
			</div>
			<form action="index.php?include=konfirmasi_tambah_lamaran" method="post" enctype="multipart/form-data">
				<div class="row">
					<div class="col-md-6 form-group">
						<label for="m_lname">Nama</label>
						<input type="text" class="form-control" id="m_lname" name="nama" value="<?php if(!empty($_SESSION['nama'])){ echo $_SESSION['nama'];} ?>">
												  
						<label for="m_nik">NIK</label>
						<input type="text" class="form-control" id="m_nik" name="nik" value="<?php if(!empty($_SESSION['nik'])){ echo $_SESSION['nik'];} ?>">					  
						<label for="m_tempat">Tempat Lahir</label>
						<input type="text" class="form-control" id="m_tempat" name="tempat" value="<?php if(!empty($_SESSION['tempat'])){ echo $_SESSION['tempat'];} ?>">
												  
						<label for="m_tanggal">Tanggal Lahir</label>
						<input type="text" class="form-control" id="m_tanggal" name="tanggal" value="<?php if(!empty($_SESSION['tanggal'])){ echo $_SESSION['tanggal'];} ?>">
							  
						<label for="m_agama">Agama</label>
						<input type="text" class="form-control" id="m_agama" name="agama" value="<?php if(!empty($_SESSION['agama'])){ echo $_SESSION['agama'];} ?>">			
						
						<br>
						<button type="submit" class="btn btn-info btn-primary btn-lg ftco-animate">
						  Daftar
						</button>                       
					</div>
					<div class="col-md-6 form-group">
						<label for="m_jk">Jenis Kelamin</label>
						<input type="text" class="form-control" id="m_jk" name="jk" value="<?php if(!empty($_SESSION['jk'])){ echo $_SESSION['jk'];} ?>">
												  
						<label for="m_email">Email</label>
						<input type="email" class="form-control" id="m_email" name="email" value="<?php if(!empty($_SESSION['email'])){ echo $_SESSION['email'];} ?>">
												  
						<label for="m_phone">Nomor Telepon</label>
						<input type="text" class="form-control" id="m_phone" name="phone" value="<?php if(!empty($_SESSION['phone'])){ echo $_SESSION['phone'];} ?>">
												  
						<label for="m_pendidikan">Pendidikan Terakhir</label>
						<input type="text" class="form-control" id="m_pendidikan" name="pendidikan" value="<?php if(!empty($_SESSION['pendidikan'])){ echo $_SESSION['pendidikan'];} ?>">
							  
						<label for="m_pekerjaan">Pengalaman Pekerjaan</label>
						<input type="text" class="form-control" id="m_pengalaman" name="pengalaman" value="<?php if(!empty($_SESSION['pengalaman'])){ echo $_SESSION['pengalaman'];} ?>">				
					</div>
				</div>
			</form>
        </div>
    </section>
    <!-- End News -->
</main>
