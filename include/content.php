<main id="main">
	<!-- ======= About US======= -->
	<section id="about" class="about section-bg">
		<div class="container" data-aos="fade-up">
			<div class="section-title">
				<h2>About Us</h2>
				<h3>
					Find Out More 
					<span>About Us</span>
				</h3>
			</div>
			<div class="row">
				<div class="col-lg-6" data-aos="zoom-out" data-aos-delay="100">
					<img src="assets/img/DD.jpg" class="img-fluid" alt="">
				</div>
				<?php 
					$sql_h = "select `kolom1`,`jasa`,`pendiri`,`kolom2` from `about`"; 
					$query_h = mysqli_query($koneksi,$sql_h);
					$i=0;
					while($data_h = mysqli_fetch_array($query_h)){
					  $kolom1 = $data_h['kolom1'];
					  $jasa = $data_h['jasa'];
					  $pendiri = $data_h['pendiri'];
					  $kolom2=$data_h['kolom2'];
				?>
				<div class="col-lg-6 pt-4 pt-lg-0 content d-flex flex-column justify-content-center" data-aos="fade-up" data-aos-delay="100">
					<h3>PT Lancar Jaya</h3>
					<p class="font-italic">
						<?php echo $kolom1?>
					</p>
					<ul>
						<li>
							<i class="icofont-ui-browser"></i>
							<div>
								<h5>Jasa</h5>
								<p><?php echo $jasa?></p>
							</div>
						</li>
						<li>
							<i class="icofont-users-alt-5"></i>
							<div>
								<h5>Pendiri</h5>
								<p><?php echo $pendiri?></p>
							</div>
						</li>
					</ul>
					<p>
						<?php echo $kolom2?>
					</p>
				</div>
				<?php
					}
				?>
			</div>
		</div>
    </section>
    <!-- End About us Section -->

    <!-- ======= Visi Misi ======= -->
    <section id="visi&misi" class="testimonials">
		<div class="container" data-aos="zoom-in">
			<div class="owl-carousel testimonials-carousel">
				<div class="testimonial-item">
					<h3>Visi &amp; Misi</h3>
					<h4>Visi</h4>
					<p>
						<i class="bx bxs-quote-alt-left quote-icon-left"></i>
							Menjadi Perusahaan Terpecaya dan Terbesar Di Indonesia,
							Menjadi Perusahaan IT Profesional dengan solusi dan layanan yang optimal serta memiliki daya saing,
							Memberikan Layanan dan Solusi yang terintegerasi dan mengikuti perkembangan dunia Teknologi Informasi.
						<i class="bx bxs-quote-alt-right quote-icon-right"></i>
					</p>
				</div>
				<div class="testimonial-item">
					<h3>Visi &amp; Misi</h3>
					<h4>Misi</h4>
					<p>
						<i class="bx bxs-quote-alt-left quote-icon-left"></i>
							Meningkatkan Kemampuan Manusia dan Organisasi Melalu berbagi Produk Inovasi dengan berbasis IT,
							Memberikan layanan yang terpadu dalam setiap layanan Teknologi Informasi yang kami berikan,
							Memberikan produk dan layanan yang berkualitas dengan layanan purna jual yang maksimal kepada setiap pelangan kami.
						<i class="bx bxs-quote-alt-right quote-icon-right"></i>
					</p>
				</div>
			</div>
		</div>
    </section>
    <!-- End VISI MISI -->

    <!-- ======= Team Section ======= -->
    <section id="team" class="team section-bg">
		<div class="container" data-aos="fade-up">
			<div class="section-title">
				<h2>Team</h2>
				<h3>Our <span>Team</span></h3>
				<p>Kekuatan terbesar seorang karyawan terletak pada kemampuannya bekerja sama dalam tim</p>
			</div>
			<div class="row">
				<?php 
					$sql_h = "select * from `team`"; 
					$query_h = mysqli_query($koneksi,$sql_h);
					$i=0;
					while($data_h = mysqli_fetch_array($query_h)){
					  $nama = $data_h['nama'];
					  $pekerjaan = $data_h['pekerjaan'];
					  $gambar = $data_h['gambar'];
				 ?>
				<div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
					<div class="member">
						<div class="member-img">
							<img src="assets/img/team/<?php echo $gambar?>" class="img-fluid" alt="">
							<div class="social">
								<a href=""><i class="icofont-twitter"></i></a>
								<a href=""><i class="icofont-facebook"></i></a>
								<a href=""><i class="icofont-instagram"></i></a>
								<a href=""><i class="icofont-linkedin"></i></a>
							</div>
						</div>
						<div class="member-info">
							<h4><?php echo $nama?></h4>
							<span><?php echo $pekerjaan?></span>
						</div>
					</div>
				</div> 
				<?php }?>
			</div>
		</div>
    </section>
    <!-- End Team Section -->

    <!-- ======= Cabang ======= -->
    <section id="cabang" class="services">
		<div class="container" data-aos="fade-up">
			<div class="section-title">
				<h2>Cabang</h2>
				<h3>Cabang <span>Perusahaan</span></h3>
				<p>PT Lancar Jaya Memiliki Berbagai Cabang Perusahaan Yang Tersebar Diseluruh Kota Yang Ada Di Indonesia</p>
			</div>
			<br><br>
			<?php 
				$sql = "select * from `cabang`"; 
				$query = mysqli_query($koneksi,$sql);
				$i=0;
				while($data_h = mysqli_fetch_array($query)){
				$nama = $data_h['nama_cabang'];
				$alamat = $data_h['alamat'];
				$email = $data_h['email'];
				$hp = $data_h['no_hp'];
				if($i%3==0){
			?>
					<div class="row">
			<?php
            	}
       		?>
				<div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
					<div class="icon-box">
						<div class="icon">
							<i class="icofont-building"></i>
						</div>
						<h4>
							<a href=""><?php echo $nama?></a>
						</h4>
						<p>
							<i class="icofont-map"></i>
							<?php echo $alamat?>
						</p>
						<p>
							<i class="icofont-mail"></i>
							<?php echo $email?>
						</p>
						<p>
							<i class="icofont-live-support"></i>
							<?php echo $hp?>
						</p>
					</div>
				</div>	
			<?php
            		$i++;
            		if ($i%3==0){
        	?>
					</div>
					<br><br>
			<?php
            		}
          		}if ($i%3!=0){
       		?>
        </div>
        <br><br>
    		<?php
				}
    		?>
    </section>
    <!-- End Cabang -->

    <!-- ======= Departemen ======= -->
    <section id="departemen" class="featured-services">
		<div class="container" data-aos="fade-up">
			<div class="section-title">
				<h2>Departemen</h2>
				<h3>Departemen <span>Perusahaan</span></h3>
				<p>Berbagai Macam Departemen Berusaha Bahu Membahu Membangun PT Lancar Jaya</p>
			</div>
			<?php 
				$sql = "select `kode_departemen`,`nama_departemen`,`deskripsi` from `departemen`"; 
				$query = mysqli_query($koneksi,$sql);
				$i=0;
				while($data_h = mysqli_fetch_array($query)){
				$kode_departemen = $data_h['kode_departemen'];
				$nama = $data_h['nama_departemen'];
				$deskripsi = $data_h['deskripsi'];
				if($i%4==0){
			?>
					<div class="row">
			<?php
            	}
       		?>
						<div class="col-md-5 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
							<div class="icon-box" data-aos="fade-up" data-aos-delay="100">
								<div class="icon">
									<i class="icofont-institution"></i>
								</div>
								<h4 class="title">
									<a href="#"><?php echo $nama?></a>
								</h4>
								<p class="description"><?php echo $deskripsi?></p>
							</div>
						</div>
			<?php
            		$i++;
            		if ($i%4==0){
        	?>
					</div>
					<br><br>
			<?php
            		}
          		}if ($i%4!=0){
       		?>
        </div>
        <br><br>
    		<?php
				}
    		?>
    </section>
    <!-- End Departemen -->

    <!-- ======= news ======= -->
    <section id="news" class="pricing">
		<div class="container" data-aos="fade-up">
			<div class="section-title">
				<h2>NEWS</h2>
				<h3>Main <span>News</span></h3>
				<p>Berita Tentang Pembukaan Lowongan Pada Perusahaan PT Lancar Jaya</p>
			</div>
        <?php 
            $sql_h = "select `kode_lamaran`,`syarat`,`bagian`,`kode_cabang` from `tabel_lamaran`"; 
            $query_h = mysqli_query($koneksi,$sql_h);
            $i=0;
            while($data_h = mysqli_fetch_array($query_h)){
              $kode_lamaran = $data_h['kode_lamaran'];
              $syarat = $data_h['syarat'];
              $bagian = $data_h['bagian'];
              $kode_cabang=$data_h['kode_cabang'];
              if($i%4==0){?>
                <div class="row">
        <?php
            }
        ?>
            <div class="col-lg-3 col-md-6" data-aos="fade-up" data-aos-delay="100">
                <div class="box featured">
                    <?php
						$sql = "select `kode_cabang`,`nama_cabang` from `cabang`"; 
						$query = mysqli_query($koneksi,$sql);
						while($data_h = mysqli_fetch_row($query)){
							$kode_c=$data_h[0];
							$nama_cabang=$data_h[1];
							if($kode_cabang==$kode_c){
                    ?>
								<h3><?php echo $nama_cabang?></h3>
                    <?php
							}
						}
                    ?>
								<h4>
									<span><?php echo $bagian?></span>
								</h4>
								<ul>
									<li><?php echo $syarat?></li>
								</ul>
								<div class="btn-wrap">
									<a href="index.php?include=lamaran" class="btn-buy">Daftar</a>
								</div>
                </div>
            </div>
        <?php
            $i++;
            if ($i%4==0){
        ?>
                </div> 
                <br><br>
        <?php
            }
          }if ($i%4!=0){
       ?>
        </div>
        <br><br>
    <?php
			}
    ?>
    </section>
    <!-- End News -->
    <!-- ======= Skills Section ======= -->
    <section id="skills" class="skills">
		<div class="container" data-aos="fade-up">
			<div class="row skills-content">
				<div class="col-lg-6">
					<div class="progress">
						<span class="skill">HTML <i class="val">100%</i></span>
						<div class="progress-bar-wrap">
							<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
					<div class="progress">
						<span class="skill">CSS <i class="val">90%</i></span>
						<div class="progress-bar-wrap">
							<div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
					<div class="progress">
						<span class="skill">JavaScript <i class="val">75%</i></span>
						<div class="progress-bar-wrap">
							<div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="progress">
						<span class="skill">PHP <i class="val">80%</i></span>
						<div class="progress-bar-wrap">
							<div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
					<div class="progress">
						<span class="skill">WordPress/CMS <i class="val">90%</i></span>
						<div class="progress-bar-wrap">
							<div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
					<div class="progress">
						<span class="skill">Photoshop <i class="val">55%</i></span>
						<div class="progress-bar-wrap">
							<div class="progress-bar" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </section>
    <!-- End Skills Section -->

    <!-- ======= Counts Section ======= -->
    <section id="counts" class="counts">
		<div class="container" data-aos="fade-up">
			<div class="row">
				<div class="col-lg-3 col-md-6">
					<div class="count-box">
						<i class="icofont-simple-smile"></i>
						<span data-toggle="counter-up">232</span>
						<p>Happy Clients</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 mt-5 mt-md-0">
					<div class="count-box">
						<i class="icofont-document-folder"></i>
						<span data-toggle="counter-up">521</span>
						<p>Projects</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 mt-5 mt-lg-0">
					<div class="count-box">
						<i class="icofont-live-support"></i>
						<span data-toggle="counter-up">1,463</span>
						<p>Hours Of Support</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 mt-5 mt-lg-0">
					<div class="count-box">
						<i class="icofont-users-alt-5"></i>
						<span data-toggle="counter-up">15</span>
						<p>Hard Workers</p>
					</div>
				</div>
			</div>
		</div>
    </section>
    <!-- End Counts Section -->
</main>