<!-- ======= Footer ======= -->
<footer id="footer">
    <div class="container py-4">
        <div class="copyright">
            &copy; Copyright 
            <strong>
                <span>PT Lancar Jaya</span>
            </strong>. All Rights Reserved
        </div>
        <div class="credits">
            Designed by <a href="https://bootstrapmade.com/">Kelompok 1</a>
        </div>
    </div>
</footer>
<!-- End Footer -->