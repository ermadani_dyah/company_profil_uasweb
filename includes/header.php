<!-- ======= Top Bar ======= -->
<div id="topbar" class="d-none d-lg-flex align-items-center fixed-top">
    <div class="container d-flex">
        <div class="contact-info mr-auto">
            <i class="icofont-envelope"></i> 
            <a href="#">pt.lancar_jaya09@gmail.com</a>
            <i class="icofont-phone"></i> +6281555332712
        </div>
        <div class="social-links">
            <a href="#" class="twitter">
            <i class="icofont-twitter"></i>
            </a>
            <a href="#" class="facebook">
            <i class="icofont-facebook"></i>
            </a>
            <a href="#" class="instagram">
            <i class="icofont-instagram"></i>
            </a>
            <a href="#" class="skype">
            <i class="icofont-skype"></i>
            </a>
            <a href="#" class="linkedin">
            <i class="icofont-linkedin"></i>
            </a>
        </div>
    </div>
</div>

<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
   <div class="container d-flex align-items-center">
        <h1 class="logo mr-auto">
            <a href="index.html">PT Lancar Jaya</a>
        </h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt=""></a>-->
		<nav class="nav-menu d-none d-lg-block">
			<ul>
                <li>
                    <a href="#home">Home</a>
                </li>
                <li>
                    <a href="#about">About Us</a>
                </li>
                <li>
                    <a href="#team">Team</a>
                </li>
                <li>
                    <a href="#cabang">Cabang</a>
                </li>
                <li>
                    <a href="#departemen">Departemen</a>
                </li>
                <li>
                    <a href="#news">News</a>
                </li>
            </ul>
         </nav>
        <!-- .nav-menu -->
    </div>
</header>
<!-- End Header -->
	
<!-- ======= Hero Section ======= -->
    <section id="hero" class="d-flex align-items-center">
        <div class="container" data-aos="zoom-out" data-aos-delay="100">
            <h1>Welcome To <span>PT Lancar Jaya</span></h1>
            <h2>Selalu Ada Risiko Dalam Sebuah Keputusan, Tetapi Selalu Ada Solusi Dalam Setiap Risiko</h2>
        </div>
    </section>
<!-- End Hero -->
